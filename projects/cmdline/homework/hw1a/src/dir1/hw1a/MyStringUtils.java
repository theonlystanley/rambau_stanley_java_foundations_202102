package hw1a;

public class MyStringUtils{
	
	public static String reverse(String str){
		
		StringBuilder mystring = new StringBuilder();
		
        mystring.append(str);
        mystring = mystring.reverse();
 
        return mystring.toString();
	}
}