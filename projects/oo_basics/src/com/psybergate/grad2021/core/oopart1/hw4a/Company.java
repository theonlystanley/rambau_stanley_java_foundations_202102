package com.psybergate.grad2021.core.oopart1.hw4a;

public class Company extends Customer {

    private String companyNum;

    public Company(String customerNum, String companyNum, String name, String customerType) {
        super(customerNum, name, customerType);
        this.companyNum = companyNum;
    }
}
