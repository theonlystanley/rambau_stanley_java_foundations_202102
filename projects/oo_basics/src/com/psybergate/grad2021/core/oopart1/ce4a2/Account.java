package com.psybergate.grad2021.core.oopart1.ce4a2;

public abstract class Account {

    protected String accountNum;

    public int balance;

    public Account(String accountNum, int balance) {
        this.accountNum = accountNum;
        this.balance = balance;
    }

    public abstract boolean isOverdrawn();

    public abstract boolean needsToBeReviewed();

    public String getAccountType() {
        return "Account";
    }

}
