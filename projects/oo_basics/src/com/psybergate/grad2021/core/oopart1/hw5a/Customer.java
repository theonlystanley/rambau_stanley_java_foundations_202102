package com.psybergate.grad2021.core.oopart1.hw5a;

import java.util.ArrayList;
import java.util.List;

public class Customer {

  private String customerNum;

  private List accounts = new ArrayList<>();

  public Customer(String customerNum) {
    this.customerNum = customerNum;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public List getAccounts() {
    return accounts;
  }

  public void addAccount(Account account) {
    accounts.add(account);
  }

  @Override
  public String toString() {
    return "Customer{" +
            "customerNum='" + customerNum + '\'' +
            ", accounts=" + accounts +
            '}';
  }
}
