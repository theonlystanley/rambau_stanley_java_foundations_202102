package com.psybergate.grad2021.core.oopart1.ce3a;

public class CurrentAccount {

    private String name;

    public CurrentAccount(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
