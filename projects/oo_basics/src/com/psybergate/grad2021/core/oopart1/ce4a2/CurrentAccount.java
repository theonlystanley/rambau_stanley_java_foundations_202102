package com.psybergate.grad2021.core.oopart1.ce4a2;

public class CurrentAccount extends Account {

    private static final int MAX_OVERDRAFT = 100_000;
    private int overdraft;
    private int interestRate;

    public CurrentAccount(String accountNum, int balance, int overdraft) {
        super(accountNum, balance);
        if(overdraft > MAX_OVERDRAFT){
            throw new RuntimeException("Account cannot be created with Over Max Overdraft of 100 000");
        }
        this.overdraft = overdraft;
    }

    public boolean isOverdrawn() {
        return balance < 0;
    }

    public boolean needsToBeReviewed() {
        return isOverdrawn() && (balance > (0.2 * overdraft) || balance <= -50000);
    }

    public String getAccountType() {
        return "Current Account";
    }
}
