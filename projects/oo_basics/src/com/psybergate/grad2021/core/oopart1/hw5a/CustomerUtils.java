package com.psybergate.grad2021.core.oopart1.hw5a;

public class CustomerUtils {

    public static void main(String[] args) {
        Customer cust = new Customer("11145");
        cust.addAccount(new SavingsAccount("0003", 2000, 500));
        cust.addAccount(new CurrentAccount("0034", 4000));
        printBalance(cust);
    }

    public static void printBalance(Customer customer) {
        System.out.println("customerNum = " + customer.getCustomerNum());
        for (Object account : customer.getAccounts()) {
            Account acc = (Account) account;
            System.out.println(acc);
        }
    }
}
