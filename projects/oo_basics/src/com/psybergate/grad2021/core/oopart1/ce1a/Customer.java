package com.psybergate.grad2021.core.oopart1.ce1a;

import java.util.ArrayList;
import java.util.List;

public class Customer {

    private String customerNum;
    private String name;
    private String address;
    private List<CurrentAccount> currentAccounts = new ArrayList<>();

    public Customer(String customerNum, String name, String address) {
        this.customerNum = customerNum;
        this.name = name;
        this.address = address;
    }

    public void addCurrentAccount(CurrentAccount currentAccount){
        currentAccounts.add(currentAccount);
    }

    public int getTotal(){
        int total = 0;

        for (CurrentAccount currentAccount : currentAccounts) {
            total += currentAccount.getBalance();
        }
        return  total;
    }

}
