package com.psybergate.grad2021.core.oopart1.ce2a;

public class CurrentAccount extends Account {

    private static final int MAX_OVERDRAFT = 100_000;
    private int overdraft;
    private int interestRate;

    public boolean isOverdrawn() {
        if (balance < 0) {
            return true;
        }
        return false;
    }

    public boolean needsToBeReviewed() {
        if (isOverdrawn() && (balance > (0.2 * overdraft) || balance <= -50000)) {
            return true;
        }
        return false;
    }

    public String getAccountType() {
        return "Current Account";
    }
}
