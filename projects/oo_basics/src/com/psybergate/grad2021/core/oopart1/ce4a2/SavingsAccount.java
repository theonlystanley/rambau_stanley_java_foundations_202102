package com.psybergate.grad2021.core.oopart1.ce4a2;

public class SavingsAccount extends Account {

    public static final int MIN_BALANCE = 5000;

    public SavingsAccount(String accountNum, int balance) {
        super(accountNum, balance);
        if (this.balance < MIN_BALANCE) {
            throw new IllegalStateException("Account must have a minimum balance of R5000");
        }
    }

    public boolean isOverdrawn() {
        return balance < 5000;
    }

    public boolean needsToBeReviewed() {
        return balance < 2000;
    }

    public String getAccountType() {
        return "Savings Account";
    }
}
