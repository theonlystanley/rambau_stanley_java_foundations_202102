package com.psybergate.grad2021.core.oopart1.ce4a1;

public class SavingsAccount extends Account{

    public String doSomething(){
        return "Method From Savings Account executing";
    }

    public String anotherMethod(){
        return "Am only available in Savings Account";
    }
}
