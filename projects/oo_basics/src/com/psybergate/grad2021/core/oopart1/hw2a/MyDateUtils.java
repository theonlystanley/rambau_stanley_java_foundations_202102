package com.psybergate.grad2021.core.oopart1.hw2a;

import java.util.Date;

public class MyDateUtils {

    public static void main(String[] args) {

        //Object orientation
        MyDateOO dateOO = new MyDateOO(new Date());
        dateOO.addDays(5);
        dateOO.validateDate();
        dateOO.compareDates(new Date());

        //Procedural programming
        String date = "2020/02/12";
        MyDatePP.validateDate(date);
        MyDatePP.addDays(date, 5);
//        MyDatePP.compareDates(date, "2020/01/30");
    }

}
