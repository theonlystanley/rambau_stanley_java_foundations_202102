package com.psybergate.grad2021.core.oopart1.hw4a;

public class Individual extends Customer {

    private static final int MIN_AGE = 18;

    private int age;

    public Individual(String customerNum, String name, String customerType) {
        super(customerNum, name, customerType);
        this.age = calculateAge();
    }

    private static int calculateAge() {
        // Calculate age
        return 18;
    }
}
