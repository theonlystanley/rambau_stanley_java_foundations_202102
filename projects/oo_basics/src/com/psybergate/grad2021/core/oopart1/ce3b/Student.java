package com.psybergate.grad2021.core.oopart1.ce3b;

public class Student extends Person {

    private String StudentNum;

    public Student(String name, String surname) {
        this(name, surname, "0000");
    }

    public Student(String name, String surname, String studentNum) {
        super(name, surname);
        this.StudentNum = studentNum;
    }

    @Override
    public int getAge() {
        return super.getAge() + 1;
    }

    public static void main(String[] args) {
        Student student =  new Student("Stanley", "Rambau" , "123456789");
        System.out.println("student.getAge() = " + student.getAge());
    }
}
