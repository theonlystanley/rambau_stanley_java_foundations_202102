package com.psybergate.grad2021.core.oopart1.hw5a;

public class SavingsAccount extends Account {

    private int minBalance;

    public SavingsAccount(String accountNum, int balance, int minBalance) {
        super(accountNum, balance);
        this.minBalance = minBalance;
    }

    @Override
    public String toString() {
        return "SavingsAccount{" +
                "minBalance=" + minBalance +
                '}';
    }
}
