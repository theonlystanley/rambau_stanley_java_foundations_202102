package com.psybergate.grad2021.core.oopart1.hw5a;

public class CurrentAccount extends Account {

    private int overdraft;

    public CurrentAccount(String accountNum, int balance) {
        super(accountNum, balance);
        this.overdraft = 1000;
    }

    @Override
    public String toString() {
        return "CurrentAccount{" +
                "overdraft=" + overdraft +
                '}';
    }
}
