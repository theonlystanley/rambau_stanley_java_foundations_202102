package com.psybergate.grad2021.core.oopart1.ce1a;

public class Utility {

    public static void main(String[] args) {
        System.out.println("Utility.customerTotal() = " + Utility.customerTotal());
    }

    public static int customerTotal() {
        Customer customer = new Customer("01", "Stanley", "Johannesburg");
        customer.addCurrentAccount(new CurrentAccount("001", 2000));
        customer.addCurrentAccount(new CurrentAccount("002", 1000));

        return customer.getTotal();
    }

}
