package com.psybergate.grad2021.core.oopart1.hw4a;

public class Customer {

    private String CustomerNum;

    private String name;

    private String CustomerType;

    public Customer(String customerNum, String name, String customerType) {
        CustomerNum = customerNum;
        this.name = name;
        CustomerType = customerType;
    }

    public String getCustomerNum() {
        return CustomerNum;
    }

    public String getName() {
        return name;
    }

    public String getCustomerType() {
        return CustomerType;
    }
}
