package com.psybergate.grad2021.core.oopart1.ce3b;

public class Person {

    protected String name;
    protected String surname;

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public int getAge(){
        return 18;
    }
}
