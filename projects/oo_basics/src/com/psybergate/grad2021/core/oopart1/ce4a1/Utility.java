package com.psybergate.grad2021.core.oopart1.ce4a1;

public class Utility {

    public static void main(String[] args) {
        Account ac = new SavingsAccount();
        System.out.println("ac.doSomething() = " + ac.doSomething()); // at runtime is will run the method in Savings account
//        ac.another() // this will not compile coz another method is only founder in savings account and the compiler has no knowledge of it
        ((SavingsAccount)ac).anotherMethod(); // This forces the compiler to work on another reference
    }
}
