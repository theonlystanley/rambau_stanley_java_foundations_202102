package com.psybergate.grad2021.core.oopart1.hw5a;

public class Account {

    private String accountNum;

    private int balance;

    public Account(String accountNum, int balance) {
        this.accountNum = accountNum;
        this.balance = balance;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public int getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNum='" + accountNum + '\'' +
                ", balance=" + balance +
                '}';
    }
}
