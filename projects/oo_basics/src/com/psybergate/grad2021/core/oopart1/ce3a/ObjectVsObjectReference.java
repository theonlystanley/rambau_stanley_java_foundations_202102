package com.psybergate.grad2021.core.oopart1.ce3a;

import java.util.ArrayList;
import java.util.List;

public class ObjectVsObjectReference {

    public static void main(String[] args) {
        List list = new ArrayList();
        list.add(new CurrentAccount("Stanley"));
        CurrentAccount ca = (CurrentAccount) list.get(0);
        ca.setName("Rambau");
        System.out.println("list.get(0) = " + ((CurrentAccount)list.get(0)).getName());
        list.set(0, ca); // This statement is not neccessary since the actually itself object has been mutated already.
    }
}
