package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OrderUtils {

  public static void main(String[] args) {

    Customer c1 = new Customer("001", "Stan", 5, Order.LOCAL_ORDER, createLocalOrders());
    Customer c2 = new Customer("002", "Rams", 5, Order.LOCAL_ORDER, createInternationalOrders());
    List<Customer> customers = new ArrayList<>();
    customers.add(c1);
    totalOrder(customers);
  }

  public static void totalOrder(List<Customer> customers) {
    for (Customer customer : customers) {
      System.out.println(customer.toString() + "\nOrders Total = " + customer.totalOrders());
    }
  }

  private static List<Order> createLocalOrders() {

    List<OrderItem> orderItems = new ArrayList<>();
    orderItems.add(new OrderItem(100, new Product("Pencils", 3, "BIG")));
    orderItems.add(new OrderItem(80, new Product("Books", 4, "P&J Trading")));

    List<Order> order = new ArrayList();
    order.add(new LocalOrder("012", orderItems, LocalDate.of(2010, 3, 12), 3));

    return order;
  }

  private static List<Order> createInternationalOrders() {

    List<OrderItem> orderItems = new ArrayList<>();
    orderItems.add(new OrderItem(100, new Product("Pencils", 3, "BIG")));
    orderItems.add(new OrderItem(80, new Product("Books", 4, "P&J Trading")));

    List<Order> order = new ArrayList();
    order.add(new InternationalOrder("123", orderItems, LocalDate.of(2010, 3, 12), 400));

    return order;
  }

}
