package com.psybergate.grad2021.core.oopart2.hw5a2_v2;

public class OrderItem {

  private Product product;

  private int quantity;

  public OrderItem(Product product, int quantity) {
    this.quantity = quantity;
    this.product = product;
  }

  public double getTotal(){
    double total = quantity * product.getPrice();
    return total;
  }


  public Product getProduct() {
    return product;
  }

  public int getQuantity() {
    return quantity;
  }

  @Override
  public String toString() {
    return "\t{"+ product +
            ", quantity=" + quantity +
            "}\n";
  }
}
