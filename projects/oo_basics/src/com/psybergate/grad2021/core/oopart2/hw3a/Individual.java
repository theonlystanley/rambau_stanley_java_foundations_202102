package com.psybergate.grad2021.core.oopart2.hw3a;

public class Individual extends Customer {

    private static final int MIN_AGE = 18;

    private int age;

    public Individual(String customerNum, String name) {
        super(customerNum, name);
        this.age = calculateAge();
    }

    private static int calculateAge() {
        // Calculate age
        return 18;
    }
}
