package com.psybergate.grad2021.core.oopart2.hw4a;

public class Rectangle extends Shape {


  public static void main(String[] args) {
    Rectangle shape = new Rectangle();

    // Static method cannot be invoked polymorphically
    shape.printColour();

    //Static Methods Are inherited
    shape.draw();
    Rectangle.draw();
  }

  public static void printColour() {
    //print colour logic
    System.out.println("Printing colour from Rectangle Class.");
  }
}
