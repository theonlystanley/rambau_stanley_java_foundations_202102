package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class LocalOrder extends Order {

  public int years;

  public LocalOrder(String orderNum, List<OrderItem> items, LocalDate date, int years) {
    super(orderNum, items, date);
    this.years = years;
  }

  @Override
  public double calculateDiscount() {
    if(years > 0 && years <= 2){
      discount = 0;
    }else if(years > 2 && years <= 5){
      discount = 0.075;
    }else if(years > 5){
      discount = 0.125;
    }
    return this.discount;
  }

  @Override
  public String toString() {
    return "LocalOrder{" +
            ", items=" + items +
            ", date=" + date +
            '}';
  }
}
