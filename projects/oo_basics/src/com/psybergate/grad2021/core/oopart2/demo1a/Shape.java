package com.psybergate.grad2021.core.oopart2.demo1a;

public class Shape {

  public static void main(String[] args) {
    Shape shape = new Rectangle();
    Rectangle rectangle = new Rectangle();
    // By observation
    // For class (or static) methods, the method according to the type of reference is called,
    // not according to the object being referred, which means method call is decided at compile time.
    // Example 1
    shape.printColour(); // print from Shape class will be called because the reference is shape.
    // Example 2
    rectangle.printColour(); // Print from Rectangle class with be called.
  }

  private int colour;

  public static void printColour(){
    //print colour logic
    System.out.println("Printing colour from Shape Class.");
  }

  public void draw(){
    //drawing shape.
    System.out.println("Drawing shape from Shape Class.");
  }

}
