package com.psybergate.grad2021.core.oopart2.hw2a;

import java.util.Objects;

public class Rectangle {

  private static final int MAX_LENGTH = 200;

  private static final int MAX_WIDTH = 100;

  private static final int MAX_AREA = 15000;

  private int length;

  private int width;

  public Rectangle(int length, int width) {
    if (length > MAX_LENGTH || width > MAX_WIDTH || length * width > MAX_AREA) {
      throw new IllegalStateException("Constraints violation detected");
    }

    this.length = length;
    this.width = width;
  }

  public int getArea() {
    return this.length * this.width;
  }

  public static int getMaxLength() {
    return MAX_LENGTH;
  }

  public static int getMaxWidth() {
    return MAX_WIDTH;
  }

  public static int getMaxArea() {
    return MAX_AREA;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null) {
      return false;
    }
    if (getClass() != o.getClass()) {
      return false;
    }
    Rectangle rectangle = (Rectangle) o;
    return length == rectangle.length && width == rectangle.width;
  }

  @Override
  public int hashCode() {
    return Objects.hash(length, width);
  }
}
