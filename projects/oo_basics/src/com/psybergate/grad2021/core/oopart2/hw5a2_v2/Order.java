package com.psybergate.grad2021.core.oopart2.hw5a2_v2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {

  public static final String LOCAL_ORDER = "Local Order";

  public static final String INTERNATIONAL_ORDER = "International Order";

  private String orderNum;

  private LocalDate orderDate;

  private Customer customer;

  private List<OrderItem> orderItems = new ArrayList<>();

  private OrderStatus orderStatus;

  public Order(String orderNum, Customer customer, OrderStatus orderStatus) {
    this.orderNum = orderNum;
    this.orderDate = LocalDate.now(); /// set the creation date to the current date.
    this.customer = customer;
    this.orderStatus = orderStatus;
  }

  public void addOrderItem(OrderItem orderItem) {
    this.orderItems.add(orderItem);
  }

  public double calculateTotal(){
    double orderTotal = 0;

    for (OrderItem orderItem : orderItems) {
      orderTotal += orderItem.getTotal();
    }

    return orderTotal;
  }

  public Customer getCustomer() {
    return customer;
  }

  @Override
  public String toString() {
    return "Order{" +
            "orderNum='" + orderNum + '\'' +
            ", createdDate=" + orderDate +
            ", \n " + customer +
            ", \n OrderItems: \n" + orderItems +
            ", \n orderStatus = " + orderStatus +
            "}\n";
  }
}
