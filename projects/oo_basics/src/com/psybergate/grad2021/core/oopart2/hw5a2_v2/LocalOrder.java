package com.psybergate.grad2021.core.oopart2.hw5a2_v2;

public class LocalOrder extends Order {

  /**
   *  Discount in percentage (decimalised).
   */
  private double discount;

  public LocalOrder(String orderNum, Customer customer, OrderStatus orderStatus) {
    super(orderNum, customer, orderStatus);
  }

  @Override
  public double calculateTotal() {
    return super.calculateTotal() * (1 + discount);
  }

  public double getDiscount() {
    return discount;
  }

}
