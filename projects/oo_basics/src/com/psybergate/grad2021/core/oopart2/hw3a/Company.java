package com.psybergate.grad2021.core.oopart2.hw3a;

public class Company extends Customer {

    private String companyNum;

    public Company(String customerNum, String companyNum, String name) {
        super(customerNum, name);
        this.companyNum = companyNum;
    }
}
