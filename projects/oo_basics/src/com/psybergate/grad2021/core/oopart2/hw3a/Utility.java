package com.psybergate.grad2021.core.oopart2.hw3a;

public class Utility {

  public static void main(String[] args) {

    Customer c1 =  new Company("0111", "1", "somename");
    Customer c2 =  new Company("0111", "2", "somename2");

    System.out.println("c1 = c2: " + (c1==c2));
    System.out.println("c1 = c2: " + (c1.equals(c2)));
  }
}
