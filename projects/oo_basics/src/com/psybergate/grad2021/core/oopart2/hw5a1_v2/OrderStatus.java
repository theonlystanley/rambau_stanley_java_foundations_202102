package com.psybergate.grad2021.core.oopart2.hw5a1_v2;

public enum OrderStatus {

  PROCESSING, DESPATCHED, DELIVERED
}

