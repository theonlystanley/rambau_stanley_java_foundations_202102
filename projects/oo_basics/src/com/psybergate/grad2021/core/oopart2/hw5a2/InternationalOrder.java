package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.time.LocalDate;
import java.util.List;

public class InternationalOrder extends Order {

  private double importDuties;

  public InternationalOrder(String orderNum, List<OrderItem> items, LocalDate date, double importDuties) {
    super(orderNum, items, date);
    this.importDuties = importDuties;
  }

  @Override
  public double getItemsTotalPrice() {
    return (super.getItemsTotalPrice() * calculateDiscount()) + importDuties;
  }

  @Override
  public double calculateDiscount() {
    if (super.getItemsTotalPrice() <= 500_000) {
      discount = 0;
    } else if (super.getItemsTotalPrice() > 500_000 && super.getItemsTotalPrice() <= 1_000_000) {
      discount = 0.05;
    } else if (super.getItemsTotalPrice() > 1_000_000) {
      discount = 0.1;
    }
    return discount;
  }

  public double getImportDuties() {
    return importDuties;
  }

  @Override
  public String toString() {
    return "InternationalOrder{" +
            "importDuties='" + importDuties + '\'' +
            ", items=" + items +
            ", date=" + date +
            '}';
  }
}
