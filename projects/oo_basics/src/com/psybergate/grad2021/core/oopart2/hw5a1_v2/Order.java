package com.psybergate.grad2021.core.oopart2.hw5a1_v2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {

  private String orderNum;

  private Customer customer;

  private LocalDate createdDate;

  private List<OrderItem> orderItems = new ArrayList<>();

//  /**
//   * Discount in percentage (decimalised).
//   */
//  protected double discount;

  public Order(String orderNum, Customer customer) {
    this.orderNum = orderNum;
    this.createdDate = LocalDate.now(); /// set the creation date to the current date.
    this.customer = customer;
  }

  public void addOrderItem(OrderItem orderItem) {
    this.orderItems.add(orderItem);
  }

  public double getDiscount(){
    return 0;
  }

  public double calculateTotal() {
    double orderTotal = 0;

    for (OrderItem orderItem : orderItems) {
      orderTotal += orderItem.getTotal();
    }

    return orderTotal;
  }

  public boolean isLocal() {
    return false;
  }

  public boolean isInternational() {
    return false;
  }

  public Customer getCustomer() {
    return customer;
  }

  @Override
  public String toString() {
    return "Order{" +
            "orderNum='" + orderNum + '\'' +
            ", createdDate=" + createdDate +
            ", \n " + customer +
            ", \n OrderItems: \n" + orderItems +
            "}\n";
  }
}
