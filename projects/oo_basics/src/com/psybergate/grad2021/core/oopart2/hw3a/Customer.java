package com.psybergate.grad2021.core.oopart2.hw3a;

public class Customer {

  private String CustomerNum;

  private String name;

  public Customer(String customerNum, String name) {
    CustomerNum = customerNum;
    this.name = name;
  }

  public String getCustomerNum() {
    return CustomerNum;
  }

  public String getName() {
    return name;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null) {
      return false;
    }
    if (getClass() != o.getClass()) {
      return false;
    }
    Customer customer = (Customer) o;
    return CustomerNum.equals(customer.CustomerNum);
  }

}
