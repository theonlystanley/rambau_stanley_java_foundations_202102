package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {

  public static final String LOCAL_ORDER = "Local Order";

  public static final String INTERNATIONAL_ORDER =  "International Order";

  protected List<OrderItem> items = new ArrayList<>();

  protected LocalDate date;

  public Order(List<OrderItem> items, LocalDate date) {
    this.items = items;
    this.date = date;
  }

  public int getItemsTotalPrice(){
    int totalPrice = 0;
    for (OrderItem item : items) {
      totalPrice += item.getQuantity() * item.getProduct().getPrice();
    }
    return totalPrice;
  }

  public List<OrderItem> getItems() {
    return items;
  }

  public LocalDate getDate() {
    return date;
  }
}
