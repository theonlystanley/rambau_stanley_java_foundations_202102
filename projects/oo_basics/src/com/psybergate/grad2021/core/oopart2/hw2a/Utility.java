package com.psybergate.grad2021.core.oopart2.hw2a;

public class Utility {

    public static void main(String[] args) {

        Rectangle r1 = new Rectangle(40, 20);
        Rectangle r2 = new Rectangle(40, 20);

        System.out.println("r1 = r2: " + (r1 == r2));
        System.out.println("r1 = r2: " + (r1.equals(r2)));
    }
}
