package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.util.ArrayList;
import java.util.List;

public class Customer {

  private String CustomerNum;

  private String name;

  private int yearWithTheCompany;

  private String customerType;

  private List<Order> orders = new ArrayList<>();

  public Customer(String customerNum, String name, int yearWithTheCompany, String customerType, List<Order> orders) {
    CustomerNum = customerNum;
    this.name = name;
    this.yearWithTheCompany = yearWithTheCompany;
    this.customerType = customerType;
    this.orders = orders;
  }

  public List<Order> getOrders() {
    return orders;
  }

  public int totalOrders() {
    int total = 0;
    for (Order order : orders) {
      total += order.getItemsTotalPrice();
    }

    return total;
  }

  @Override
  public String toString() {
    return "Customer{" +
            "CustomerNum='" + CustomerNum + '\'' +
            ", name='" + name + '\'' +
            ", yearWithTheCompany=" + yearWithTheCompany +
            ", customerType='" + customerType + '\'' +
            '}';
  }
}
