package com.psybergate.grad2021.core.oopart2.demo2a;

public class Account {

  protected String accountNum;

  protected int balance;

  public Account(String accountNum, int balance) {
    this.accountNum = accountNum;
    this.balance = balance;
  }
}
