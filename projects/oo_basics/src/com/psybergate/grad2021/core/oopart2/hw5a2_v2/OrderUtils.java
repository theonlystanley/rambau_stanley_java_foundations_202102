package com.psybergate.grad2021.core.oopart2.hw5a2_v2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class OrderUtils {

  public static void main(String[] args) {

    List<Order> orders = systemDB();

    for (Order order : orders) {
      System.out.println(order);
    }

  }

  public static void totalOrder(List<Customer> customers) {

  }

  private static List<Order> systemDB() {

    List<OrderItem> orderItems = createOrderItems();

    List<Order> orders = createOrders();

    addingOrderItemsIntoOrders(orderItems, orders);

    return orders;
  }

  private static void addingOrderItemsIntoOrders(List<OrderItem> orderItems, List<Order> orders) {

    Random rand = new Random();
    for (Order order : orders) {
      int randomNum = rand.nextInt(4) + 1;
      for (int i = 0; i < randomNum; i++) {
        int index = rand.nextInt(5);
        order.addOrderItem(orderItems.get(index));
      }
    }
  }

  private static List<Order> createOrders() {
    // Creating Local Orders
    Customer c1 = new Customer("001", "Stan", "Rams", Customer.LOCAL_CUSTOMER);
    Customer c2 = new Customer("002", "Sebastian", "Monroe", LocalDate.of(2016, 01, 20), Customer.LOCAL_CUSTOMER);

    Order o1 = new LocalOrder("01", c1, OrderStatus.PROCESSING);
    Order o2 = new LocalOrder("02", c2, OrderStatus.PROCESSING);
    Order o3 = new LocalOrder("03", c1, OrderStatus.DISPATCHED);
    Order o4 = new LocalOrder("04", c1, OrderStatus.DELIVERED);
    Order o5 = new LocalOrder("05", c2, OrderStatus.DELIVERED);

    /// Creating international orders
    Customer c3 = new Customer("001", "Stan", "Rams", Customer.INTERNATIONAL_CUSTOMER);
    Customer c4 = new Customer("001", "Stan", "Rams", LocalDate.of(2017, 01, 20), Customer.LOCAL_CUSTOMER);

    Order o6 = new LocalOrder("01", c3, OrderStatus.PROCESSING);
    Order o7 = new LocalOrder("02", c4, OrderStatus.PROCESSING);
    Order o8 = new LocalOrder("03", c4, OrderStatus.DISPATCHED);
    Order o9 = new LocalOrder("04", c3, OrderStatus.DELIVERED);
    Order o10 = new LocalOrder("05", c4, OrderStatus.DELIVERED);

    List<Order> orders = new ArrayList<>();
    orders.add(o1);
    orders.add(o2);
    orders.add(o3);
    orders.add(o4);
    orders.add(o5);
    orders.add(o6);
    orders.add(o7);
    orders.add(o8);
    orders.add(o9);
    orders.add(o10);

    return orders;
  }

  private static List<OrderItem> createOrderItems() {
    List<OrderItem> orderItems = new ArrayList<>();

    Product p1 = new Product("Shirts", 350, "NIKE");
    Product p2 = new Product("Sneakers", 850, "VANS");
    Product p3 = new Product("T-Shirts", 280, "TFG");
    Product p5 = new Product("Underwear Pack", 300, "Office London");
    Product p4 = new Product("Jean", 500, "TFG");

    orderItems.add(new OrderItem(p1, 25));
    orderItems.add(new OrderItem(p3, 80));
    orderItems.add(new OrderItem(p4, 60));
    orderItems.add(new OrderItem(p5, 100));
    orderItems.add(new OrderItem(p5, 40));

    return orderItems;
  }
}
