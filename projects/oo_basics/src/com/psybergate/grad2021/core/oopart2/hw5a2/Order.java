package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public abstract class Order {

  public static final String LOCAL_ORDER = "Local Order";

  public static final String INTERNATIONAL_ORDER = "International Order";

  private String orderNum;

  protected List<OrderItem> items = new ArrayList<>();

  protected double discount;

  protected LocalDate date;

  public Order(String orderNum, List<OrderItem> items, LocalDate date) {
    this.items = items;
    this.orderNum = orderNum;
    this.date = date;
  }

  public double getItemsTotalPrice() {
    int totalPrice = 0;
    for (OrderItem item : items) {
      totalPrice += item.getQuantity() * item.getProduct().getPrice();
    }
    return totalPrice;
  }

  public abstract double calculateDiscount();

  public List<OrderItem> getItems() {
    return items;
  }

  public LocalDate getDate() {
    return date;
  }
}
