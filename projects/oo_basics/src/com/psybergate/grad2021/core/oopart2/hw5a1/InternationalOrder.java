package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.time.LocalDate;
import java.util.List;

public class InternationalOrder extends Order {

  private double importDuties;

  public InternationalOrder(List<OrderItem> items, LocalDate date, double importDuties) {
    super(items, date);
    this.importDuties = importDuties;
  }

  public double getImportDuties() {
    return importDuties;
  }

  @Override
  public int getItemsTotalPrice() {
    return super.getItemsTotalPrice() + (int)importDuties;
  }

  @Override
  public String toString() {
    return "InternationalOrder{" +
            "importDuties='" + importDuties + '\'' +
            ", items=" + items +
            ", date=" + date +
            '}';
  }
}
