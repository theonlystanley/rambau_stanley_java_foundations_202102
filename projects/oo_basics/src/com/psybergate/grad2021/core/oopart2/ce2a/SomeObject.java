package com.psybergate.grad2021.core.oopart2.ce2a;

public class SomeObject {

  private int someVariable;

  public SomeObject(int someVariable) {
    this.someVariable = someVariable;
  }

  public void someMethod(){

    SomeObject a = new SomeObject(5);
    SomeObject b = new SomeObject(8);

    System.out.println("a.getSomeVariable() = " + a.getSomeVariable());
    System.out.println("b.getSomeVariable() = " + b.getSomeVariable());

    swap(a,b);

    System.out.println("a.getSomeVariable() After Swap = " + a.getSomeVariable());
    System.out.println("b.getSomeVariable() After Swap = " + b.getSomeVariable());

  }

  public void swap(SomeObject a, SomeObject b){
    // it is not possible to swap a and b.
    SomeObject temp = a;
    a = b;
    b = temp;

  }

  public int getSomeVariable() {
    return someVariable;
  }
}
