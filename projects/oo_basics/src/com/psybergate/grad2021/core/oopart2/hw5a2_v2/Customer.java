package com.psybergate.grad2021.core.oopart2.hw5a2_v2;

import java.time.LocalDate;

public class Customer {

  public static final String LOCAL_CUSTOMER = "Local";

  public static final String INTERNATIONAL_CUSTOMER = "International";

  private String customerNum;

  private String customerName;

  private String surname;

  private LocalDate startDate;

  private String customerType;

  public Customer(String customerNum, String customerName, String surname, String customerType) {
    this(customerNum, customerName, surname, LocalDate.now(), customerType); /// This one defaults the start date to the current date.
  }

  public Customer(String customerNum, String customerName, String surname, LocalDate startDate, String customerType) {
    this.customerNum = customerNum;
    this.customerName = customerName;
    this.surname = surname;
    this.startDate = startDate; // Set the creation date to the current date
    this.customerType = customerType;
  }

  @Override
  public String toString() {
    return "Customer{" +
            ", customerName='" + customerName + '\'' +
            ", surname='" + surname + '\'' +
            '}';
  }
}

