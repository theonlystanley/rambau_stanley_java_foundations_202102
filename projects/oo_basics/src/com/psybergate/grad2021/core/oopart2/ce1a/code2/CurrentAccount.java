package com.psybergate.grad2021.core.oopart2.ce1a.code2;

public class CurrentAccount extends Account {

  public static void main(String[] args) {
    Account account = new CurrentAccount("0123", 2000, 5000);
  }

  private int overdraft;

  private static final int MAX_OVERDRAFT = 10_000;

  public CurrentAccount(String accountNum, int balance, int overdraft) {
    super(accountNum, balance);
    this.overdraft = overdraft;
    System.out.println("overdraft = " + overdraft);
  }
}
