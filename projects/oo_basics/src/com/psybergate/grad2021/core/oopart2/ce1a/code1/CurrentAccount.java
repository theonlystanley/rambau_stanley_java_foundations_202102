package com.psybergate.grad2021.core.oopart2.ce1a.code1;

public class CurrentAccount extends Account {

  public CurrentAccount(String accountNum) {
    super(accountNum); // if we do not call and pass accountNum to parent constructor the code wont compile.
  }
}
