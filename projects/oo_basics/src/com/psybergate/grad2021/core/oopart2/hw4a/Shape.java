package com.psybergate.grad2021.core.oopart2.hw4a;

public class Shape {

  private int colour;

  public static void printColour(){
    //print colour logic
    System.out.println("Printing colour from Shape Class.");
  }

  public static void draw(){
    //drawing shape.
    System.out.println("Drawing shape from Shape Class.");
  }

}
