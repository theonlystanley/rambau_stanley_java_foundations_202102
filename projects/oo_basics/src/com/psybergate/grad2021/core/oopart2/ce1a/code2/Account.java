package com.psybergate.grad2021.core.oopart2.ce1a.code2;

public class Account {

  private String accountNum;

  private int balance;

  public Account(String accountNum, int balance) {
    this.accountNum = accountNum;
    this.balance = balance;
    System.out.println("accountNum = " + accountNum + ", balance = " + balance);

  }
}
