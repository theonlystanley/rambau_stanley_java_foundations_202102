package com.psybergate.grad2021.core.oopart2.hw5a1_v2;

import com.psybergate.grad2021.core.oopart2.hw5a2_v2.CustomerType;

public class InternationalOrder extends Order {

  public static final String INTERNATIONAL_ORDER = "International Order";
  /**
   *  Amount in Rands
   */
  private double importDuties;

  public InternationalOrder(String orderNum, Customer customer, OrderStatus orderStatus) {
    super(orderNum, customer);
    if (customer.getCustomerType() != CustomerType.INTERNATIONAL) {
      throw new RuntimeException("Invalid Customer: Only International Customer Can Order Internationally");
    }
  }

  @Override
  public boolean isInternational() {
    return false;
  }

  @Override
  public double calculateTotal() {
    return super.calculateTotal() + importDuties;
  }

  public double getImportDuties() {
    return importDuties;
  }
}
