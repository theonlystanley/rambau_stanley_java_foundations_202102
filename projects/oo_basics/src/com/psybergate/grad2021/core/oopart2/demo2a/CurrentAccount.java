package com.psybergate.grad2021.core.oopart2.demo2a;

public class CurrentAccount extends Account {

  public static void main(String[] args) {
    Account account = new CurrentAccount("0123", 2000, 5000);
    Account account1 = new CurrentAccount("0123", 2000, 5000);

    System.out.println("(account == account1) = " + (account == account1));
    System.out.println("(account.equals(account1)) = " + (account.equals(account1)));

  }

  private int overdraft;

  private static final int MAX_OVERDRAFT = 10_000;

  public CurrentAccount(String accountNum, int balance, int overdraft) {
    super(accountNum, balance);
    this.overdraft = overdraft;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null) {
      return false;
    }
    if (!(o instanceof CurrentAccount)) {
      return false;
    }
    CurrentAccount ca = (CurrentAccount) o;
    return accountNum == ca.accountNum;
  }

}
