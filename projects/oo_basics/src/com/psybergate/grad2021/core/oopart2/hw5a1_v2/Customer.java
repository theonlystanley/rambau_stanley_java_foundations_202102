package com.psybergate.grad2021.core.oopart2.hw5a1_v2;

import com.psybergate.grad2021.core.oopart2.hw5a2_v2.CustomerType;

import java.time.LocalDate;

public class Customer {

  private String customerNum;

  private String customerName;

  private String surname;

  private CustomerType customerType;

  private LocalDate regDate;

  public Customer(String customerNum, String customerName, String surname, CustomerType customerType) {
    this(customerNum, customerName, surname, LocalDate.now(), customerType); /// This one defaults the start date to the current date.
  }

  public Customer(String customerNum, String customerName, String surname, LocalDate regDate, CustomerType customerType) {
    this.customerNum = customerNum;
    this.customerName = customerName;
    this.surname = surname;
    this.regDate = regDate; // Set the creation date to the current date
    this.customerType = customerType;
  }

  public CustomerType getCustomerType() {
    return customerType;
  }

  @Override
  public String toString() {
    return "Customer{" +
            ", customerName='" + customerName + '\'' +
            ", surname='" + surname + '\'' +
            '}';
  }
}

