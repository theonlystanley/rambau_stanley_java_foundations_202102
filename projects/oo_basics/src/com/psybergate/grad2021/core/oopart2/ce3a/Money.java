package com.psybergate.grad2021.core.oopart2.ce3a;

import java.math.BigDecimal;

public class Money {

  public static void main(String[] args) {
    Currency currency = new Currency("South Africa", "ZAR");
    Money money = new Money(5.5, currency);
    Money m2 = new Money(10, currency);
    currency.setCountryCode("USD");

    System.out.println("money = " + money.add(m2));
    System.out.println("money = " + money);

    System.out.println(money == m2);

  }

  private BigDecimal value;
  private Currency currency;

  public Money(int value, Currency currency) {
    this((double) value, currency);
  }

  public Money(double value, Currency currency) {
    this.value = BigDecimal.valueOf(value);
    this.currency = currency.getClone();
  }

  private Money(BigDecimal value, Currency currency) {
    this.value = value;
    this.currency = currency.getClone();
  }

  public Money add(Money money) {
    return new Money(value.add(money.value), currency.getClone());
  }

  public BigDecimal getValue() {
    return value;
  }

  @Override
  public String toString() {
    return "Money{" +
            "value=" + value +
            ", currency=" + currency +
            '}';
  }
}
