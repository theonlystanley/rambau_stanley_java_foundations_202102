package com.psybergate.grad2021.core.oopart2.hw5a2_v2;

public enum OrderStatus {

  PROCESSING, DISPATCHED, DELIVERED
}

