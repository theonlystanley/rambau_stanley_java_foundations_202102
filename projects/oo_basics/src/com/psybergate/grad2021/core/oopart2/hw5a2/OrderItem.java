package com.psybergate.grad2021.core.oopart2.hw5a2;

public class OrderItem {

  private int quantity;

  private Product product;

  public OrderItem(int quantity, Product product) {
    this.quantity = quantity;
    this.product = product;
  }

  public double getTotal(){
    double total = quantity * product.getPrice();
    return total;
  }

  public Product getProduct() {
    return product;
  }

  public int getQuantity() {
    return quantity;
  }

  @Override
  public String toString() {
    return "OrderItem{" +
            "quantity=" + quantity +
            ", product=" + product +
            '}';
  }
}
