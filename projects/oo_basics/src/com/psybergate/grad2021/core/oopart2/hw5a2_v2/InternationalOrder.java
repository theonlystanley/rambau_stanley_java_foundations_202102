package com.psybergate.grad2021.core.oopart2.hw5a2_v2;

public class InternationalOrder extends Order {

  /**
   * Amount in Rands
   */
  private double importDuties;

  public InternationalOrder(String orderNum, Customer customer, OrderStatus orderStatus) {
    super(orderNum, customer, orderStatus);
  }
//
//  @Override
//  public double calculateTotal() {
//    return super.calculateTotal() + importDuties;
//  }
//
//  public double calculateDiscount() {
//    if (super.getItemsTotalPrice() <= 500_000) {
//      discount = 0;
//    } else if (super.getItemsTotalPrice() > 500_000 && super.getItemsTotalPrice() <= 1_000_000) {
//      discount = 0.05;
//    } else if (super.getItemsTotalPrice() > 1_000_000) {
//      discount = 0.1;
//    }
//    return discount;
//  }

  public double getImportDuties() {
    return importDuties;
  }
}
