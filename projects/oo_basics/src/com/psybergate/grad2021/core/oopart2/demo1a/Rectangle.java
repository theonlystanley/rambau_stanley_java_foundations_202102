package com.psybergate.grad2021.core.oopart2.demo1a;

public class Rectangle extends Shape {

  // This method is not overriding the method from the parent (superclass) class
  //@Override // if it was overriding it should comile with this overriding notation.
  public static void printColour() {
    //print colour logic
    System.out.println("Printing colour from Rectangle Class.");
  }

  public void draw() {
    //drawing shape.
    System.out.println("Drawing shape from Rectangle Class.");
  }
}
