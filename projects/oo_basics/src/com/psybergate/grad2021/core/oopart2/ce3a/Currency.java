package com.psybergate.grad2021.core.oopart2.ce3a;

public class Currency {

  private String countryName;
  private String countryCode;

  public Currency(String countryName, String countryCode) {
    this.countryName = countryName;
    this.countryCode = countryCode;
  }

  public Currency getClone(){
    return  new Currency(this.countryName, this.countryCode);
  }

  public String getCountryName() {
    return countryName;
  }

  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  @Override
  public String toString() {
    return "Currency{" +
            "countryName='" + countryName + '\'' +
            ", countryCode='" + countryCode + '\'' +
            '}';
  }
}
