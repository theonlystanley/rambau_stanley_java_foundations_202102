package com.psybergate.grad2021.core.oopart2.hw5a1;

import java.time.LocalDate;
import java.util.List;

public class LocalOrder extends Order{

  private double discount;

  public LocalOrder(List<OrderItem> items, LocalDate date) {
    super(items, date);
    this.discount = discount;
  }

  public double getDiscount() {
    return discount;
  }

  @Override
  public String toString() {
    return "LocalOrder{" +
            "discount=" + discount +
            ", items=" + items +
            ", date=" + date +
            '}';
  }
}
