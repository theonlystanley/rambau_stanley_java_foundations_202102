public class Lockers {

	public static void main(String[] args) {

		int numOfLocker = 1000;

		int[] lockers = new int[numOfLocker];

		initializeLockers(lockers, numOfLocker);

		// Student 1 opening all lockers
		for (int i = 0; i < numOfLocker; i++) {
			lockers[i] = 1;
		}

		int openedLockers = numOfLocker;
		
		openedLockers = openAndCloseLockers(lockers, numOfLocker, openedLockers);

		System.out.println("Number of Lockers Open: " + openedLockers);

	}

	private static int openAndCloseLockers(int[] lockers, int numOfLocker, int openedLockers) {
		for (int stud = 1; stud < numOfLocker; stud++) {
			for (int locker = stud; locker < numOfLocker; locker += stud + 1) {
				if (lockers[locker] == 1) {
					lockers[locker] = 0;
					openedLockers--;
				} else if (lockers[locker] == 0) {
					lockers[locker] = 1;
					openedLockers++;
				}
			}

		}
		return openedLockers;
	}

	private static void initializeLockers(int[] lockers, int size) {

		for (int i = 0; i < size; i++) {
			lockers[i] = 0;
		}

	}

}