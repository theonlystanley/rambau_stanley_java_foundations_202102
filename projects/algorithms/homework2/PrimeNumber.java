
public class PrimeNumber {

	public static void main(String[] args) {
		
		int numOfPrimes = 0;
		int range = 200000;
		
		long start = System.currentTimeMillis();
		
		for (int i = 1; i <= range; i++) {
			if(isPrime(i)) {
				numOfPrimes++;
				//System.out.print(i + " ");
			}
		}
		
		System.out.println("There are " + numOfPrimes + " prime number between 1 and " + range);
		
		long timer = System.currentTimeMillis() - start;
		
		System.out.println("Execution time: " + timer + "ms" );
	}

	private static boolean isPrime(int number) {

		int mid = 0;
		mid = number / 2;
		
		if (number == 0 || number == 1) {
			return false;
		}else if(number == 2) {
			return true;
		}else if(number == 5) {
			return true;
		}else if(number % 2 == 0) {
			return false;
		}else if(number % 10 == 5 || number % 10 == 0) {
			return false;
		}else if((number % 100) % 4 == 0) {
			return false;
		} else {
			for (int i = 3; i <= mid; i++) {
				if (number % i == 0) {
					return false;
				}
			}
			
		}
		
		return true; 
	}

}
