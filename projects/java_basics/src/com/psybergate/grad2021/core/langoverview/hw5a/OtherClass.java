package com.psybergate.grad2021.core.langoverview.hw5a;

public class OtherClass {

  public static void main(String[] args) {

    AccessModifiers ac = new AccessModifiers();

    ac.somePackageMethod();
    ac.someProtectedMethod();
    System.out.println("ac.protectedVar = " + ac.protectedVar);
    System.out.println("ac.packageVar = " + ac.packageVar);

  }

}
