package com.psybergate.grad2021.core.langoverview.hw6a;

public class StaticModifier {

  private static int classVar = 10;

  private int objectVar = 20;

  public StaticModifier() {  /// constructor cannot be static
  }

  public static void main(String[] args) {
    StaticModifier.someMethod();
    System.out.println("StaticModifier.classVar = " + StaticModifier.classVar);

  }

  private static void someMethod() {

  }
}
