package com.psybergate.grad2021.core.langoverview.hw5a.otherpackage;

import com.psybergate.grad2021.core.langoverview.hw5a.AccessModifiers;

public class DifferentPackageClass extends AccessModifiers {

  public static void main(String[] args) {

    AccessModifiers ac = new AccessModifiers();

    System.out.println("ac.publicVar = " + ac.publicVar);
//        ac.somePackageMethod();  // Cannot be accessed out side the package

    DifferentPackageClass dpc = new DifferentPackageClass();
    System.out.println("dpc.protectedVar = " + dpc.protectedVar);
    dpc.someProtectedMethod();

  }

  @Override
  protected void someProtectedMethod() {
    System.out.println("I inherited this method");
  }
}
