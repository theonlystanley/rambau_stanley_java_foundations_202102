package com.psybergate.grad2021.core.langoverview.hw5a;

public class AccessModifiers {

  private int privateVar = 1;

  int packageVar = 2;

  protected int protectedVar = 2;

  public int publicVar = 3;

  protected void someProtectedMethod() {

  }

  void somePackageMethod() {

  }
}
