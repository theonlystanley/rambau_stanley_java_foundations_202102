package com.psybergate.grad2021.core.langoverview.hw7a;

public class FinalModifier { //Final class cannot be inherited and cannot the declared as an incomplete class

  public static final int CLASS_VAR;  // Must be initialized in the static initializer block

  final int OBJECT_VAR;   // Must be initialized in the constructor

  final FinalModifier fm = new FinalModifier(); // its reference cannot be changed but the contents of the object can be changed

  static {
    CLASS_VAR = 10;
  }

  public FinalModifier() { // constructors cannot be declared final because they not inherited.
    this.OBJECT_VAR = 1;
  }

  final public void someMethod(final int parameterVar) { // Final method cannot be overridden (this is its final implementation)
    System.out.println("someMethod Executing");
//        parameterVar = 1; // you cannot do this
  }

  public static void main(String[] args) {

//        fm = new FinalModifier(); // you cannot do this to a final object

  }

}

class OtherClass extends FinalModifier {

//    public void someMethod(){ // Cannot do this if the method is final
//
//    }

}