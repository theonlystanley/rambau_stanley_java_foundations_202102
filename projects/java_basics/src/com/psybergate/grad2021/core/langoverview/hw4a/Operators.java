package com.psybergate.grad2021.core.langoverview.hw4a;

public class Operators {

  public Operators() {
  }

  public static void main(String[] args) {

    System.out.println(3 % 2);  // return the remainder

    int var = 0;
    System.out.println(++var);
    System.out.println(var++);
    System.out.println(var);

    String myString = var == 0 ? "yes" : "No";
    System.out.println("myString = " + myString);

  }

}
