package com.psybergate.grad2021.core.concurrency.hw1a.v2;

public class MessageWriter implements  Runnable{

  private Message message;
  private String texts[] = {
          "Hello",
          "How are you?",
          "How have you been?",
          "End"
  };

  public MessageWriter(Message message) {
    this.message = message;
  }

  @Override
  public void run() {
    for (String text : texts) {
      message.write(text);
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

}
