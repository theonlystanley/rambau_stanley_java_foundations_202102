package com.psybergate.grad2021.core.concurrency.ce1a;

public class MyObject {

  private int objNum;

  public MyObject(int objNum) {
    this.objNum = objNum;
  }

  public void doSomething1() {
    System.out.println("Do Something 1 from the Object.");
    doSomething2();
  }

  public void doSomething2() {
    System.out.println("Do Something 2 from the Object.");
    doSomething3();
  }

  public void doSomething3() {
    throw new RuntimeException("Something went wrong in object.");
  }
}
