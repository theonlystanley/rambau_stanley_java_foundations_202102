package com.psybergate.grad2021.core.concurrency.hw1a.v1;

public class MessageReader implements Runnable {

  private Message message;

  public MessageReader(Message message) {
    this.message = message;
  }

  @Override
  public void run() {

    for (String text = message.read(); !"End".equals(text);
         text = message.read()) {

      System.out.println(text);
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

    }
  }

}
