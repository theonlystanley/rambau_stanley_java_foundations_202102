package com.psybergate.grad2021.core.concurrency.hw1a.v2;

public class Message {

  /**
   * message text
   */
  private String text;

  /**
   * Indicate when the message has been read
   * its true if the the reader has read the text
   * and its false if reader hasn't read the text.
   */
  private boolean seen = true;

  public synchronized void write(String text){
    while (!seen) {
      threadWaiting();
    }
    seen = false;
    this.text = text;
    notify();
  }

  public synchronized String read(){
    while (seen) {
      threadWaiting();
    }
    seen = true;
    notify();
    return text;
  }

  private void threadWaiting() {
    try {
      wait();
    } catch (InterruptedException e)  {
      e.printStackTrace();
      System.exit(-1);
    }
  }
}
