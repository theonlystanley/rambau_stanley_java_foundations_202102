package com.psybergate.grad2021.core.concurrency.ce3a;

public class Worker implements Runnable{

  private Adder adder;

  public Worker(Adder adder) {
   this.adder = adder;
  }

  @Override
  public void run() {
    adder.sum();
  }
}
