package com.psybergate.grad2021.core.concurrency.hw1a.v1;

public class MessageClient {

  public static void main(String[] args) {

    Message message = new Message();
    Thread writerThread = new Thread(new MessageWriter(message), "Writer");
    Thread readerThread = new Thread(new MessageReader(message), "Reader");

    writerThread.start();
    readerThread.start();
  }
}
