package com.psybergate.grad2021.core.concurrency.hw1a.v1;

public class Message {

  /**
   * message text
   */
  private String text;

  /**
   * Indicate when the message has been read
   * its true if the the reader has read the text
   * and its false if reader hasn't read the text.
   */
  private boolean seen = true;

  public void write(String text){
    synchronized (this){
        System.out.println(Thread.currentThread() + " is writing...");
      seen = false;
      this.text = text;
    }
  }

  public String read(){
    synchronized (this){
        System.out.println(Thread.currentThread() + " is reading...");
      seen = true;
      return text;
    }
  }
}
