package com.psybergate.grad2021.core.concurrency.ce1a;

public class ThreadStackTrace {

  public static void main(String[] args) {

    Thread thread = new Thread(new ThreadWorker(new MyObject(10)));
    thread.start();
    doSomething1();
  }

  public static void doSomething1() {
    System.out.println("Do Something 1 from main.");
    doSomething2();
  }

  public static void doSomething2() {
    System.out.println("Do Something 2 from main");
    throw new RuntimeException("Something went wrong in main.");
  }

}
