package com.psybergate.grad2021.core.concurrency.ce3a;

public class Adder {

  private long startValue;
  private long endValue;

  public Adder(int startValue, int endValue) {
    this.startValue = startValue;
    this.endValue = endValue;
  }

  public long sum(){
    long sum = 0;
    for (long i = startValue; i < endValue; i++) {
      sum += 1;
    }
    return sum;
  }
}
