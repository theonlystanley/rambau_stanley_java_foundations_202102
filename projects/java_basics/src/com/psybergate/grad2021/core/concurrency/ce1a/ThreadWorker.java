package com.psybergate.grad2021.core.concurrency.ce1a;

public class ThreadWorker implements Runnable {

  private MyObject data;

  public ThreadWorker(MyObject data) {
    this.data = data;
  }

  @Override
  public void run() {
      data.doSomething1();
  }
}
