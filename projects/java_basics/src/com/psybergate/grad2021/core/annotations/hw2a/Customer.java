package com.psybergate.grad2021.core.annotations.hw2a;

@DomainClass(primaryKey = "customerNum")
public class Customer {

  @DomainProperty(name = "customerNum", nullable = false, unique = true)
  private String customerNum;

  @DomainProperty(name = "name", nullable = false)
  private String name;

  @DomainProperty(name = "surname", nullable = false)
  private String surname;

  @DomainProperty(name = "dateOfBirth", nullable = false)
  private int dateOfBirth;

  @DomainTransient
  private int age;

  public Customer(String customerNum, String name, String surname, int dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }
}
