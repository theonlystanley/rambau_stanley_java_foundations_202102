package com.psybergate.grad2021.core.annotations.hw3a;

@DomainClass(primaryKey = "customerNum")
public class Customer {

  @DomainProperty(name = "customerNum", primaryKey = true, nullable = false, unique = true)
  String customerNum;

  @DomainProperty(name = "name", nullable = false)
  String name;

  @DomainProperty(name = "surname", nullable = false)
  String surname;

  @DomainProperty(name = "dateOfBirth", nullable = false)
  int dateOfBirth;

  @DomainTransient
  int age;

  public Customer(String customerNum, String name, String surname, int dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }
}
