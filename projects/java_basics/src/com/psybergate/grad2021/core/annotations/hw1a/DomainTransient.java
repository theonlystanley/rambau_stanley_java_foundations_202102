package com.psybergate.grad2021.core.annotations.hw1a;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DomainTransient {
}
