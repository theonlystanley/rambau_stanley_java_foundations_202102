package com.psybergate.grad2021.core.annotations.dbtest;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectDB {

  public static final String ORG_POSTGRESQL_DRIVER = "org.postgresql.Driver";

  public static final String URL = "jdbc:postgresql://localhost:5432/tester";

  public static final String USERNAME = "postgres";

  public static final String PASSWORD = "admin";

  public static void main(String[] args) {

    Connection conn = null;
    try {
      Class.forName(ORG_POSTGRESQL_DRIVER);
      conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);

    } catch (Exception e) {
      e.printStackTrace();
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Opened database successfully");
  }
}
