package com.psybergate.grad2021.core.annotations.hw4a;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class Utility {

  public static void main(String[] args) throws SQLException, ClassNotFoundException, IllegalAccessException {

    System.out.println("******* Generating a Database Table: **********");
    DatabaseManager.generateDatabase();
    System.out.println("******* Database Table Generated successfully: **********");
    System.out.println("******* Creating Customer objects and saving them in the database: **********");
    List<Customer> customers = Arrays.asList(new Customer("001", "name1", "surname1", 19980102),
            new Customer("003", "name3", "surname3", 19910213),
            new Customer("004", "name4", "surname4", 19891123),
            new Customer("002", "name2", "surname2", 19950923));
    System.out.println(customers.toString());
    for (Customer customer : customers) {
      CustomerService.saveCustomer(customer);
    }
    System.out.println("******* Customer objects saved Successfully: **********");
    String custNum = "002";
    System.out.println("******* Retrieving Customer where customerNum = " + custNum + ": **********");
    Customer retrievedCustomer = CustomerService.getCustomer(custNum);
    System.out.println("******* Customer Retrieved Successfully: **********");
    System.out.println(retrievedCustomer);

  }
}
