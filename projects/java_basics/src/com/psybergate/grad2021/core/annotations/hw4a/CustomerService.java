package com.psybergate.grad2021.core.annotations.hw4a;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CustomerService {

  public static void saveCustomer(Customer customer) throws IllegalAccessException, SQLException, ClassNotFoundException {

    Class classObj = customer.getClass();
    Field[] fields = classObj.getDeclaredFields();
    String tableName = classObj.getSimpleName();

    StringBuilder query = constructInsertQuery(customer, fields, tableName);
    Connection conn = DatabaseManager.connectToDB();
    Statement statement = conn.createStatement();
    statement.executeUpdate(query.toString());
    conn.close();
  }

  public static Customer getCustomer(String customerNum) throws SQLException, ClassNotFoundException, IllegalAccessException {

    Connection conn = DatabaseManager.connectToDB();
    Statement statement = conn.createStatement();
    StringBuilder query = new StringBuilder("Select * from Customer where customerNum='" + customerNum + "'");
    ResultSet results = statement.executeQuery(query.toString());
    Customer customer = constructCustomerWithTheResults(results);
    return customer;
  }

  private static Customer constructCustomerWithTheResults(ResultSet results) throws SQLException, IllegalAccessException {
    Customer customer = new Customer();
    while (results.next()) {
      Field[] fields = Customer.class.getDeclaredFields();
      for (Field field : fields) {
        Annotation[] annotations = field.getAnnotations();
        for (Annotation annotation : annotations) {
          if (annotation instanceof DomainProperty) {
            if (field.getType().getTypeName() == String.class.getTypeName()) {
              field.set(customer, results.getString(field.getName()));
            } else {
              field.set(customer, results.getInt(field.getName()));
            }
          }
        }
      }
    }
    return customer;
  }

  private static StringBuilder constructInsertQuery(Customer customer, Field[] fields, String tableName) throws IllegalAccessException {

    StringBuilder query = new StringBuilder("INSERT INTO " + tableName + "(");
    StringBuilder values = new StringBuilder("VALUES (");
    for (Field field : fields) {
      Annotation[] annotations = field.getAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          DomainProperty dp = (DomainProperty) annotation;
          query.append(dp.name() + ", ");

          Object value = field.get(customer);
          values.append((value.getClass().getSimpleName().equals("String") ? "'" + value + "'" : value) + ", ");
        }
      }
    }
    values.setCharAt(values.length() - 2, ' ');
    values.append(");");
    query.setCharAt(query.length() - 2, ' ');
    query.append(") \n" + values.toString());
    return query;
  }
}
