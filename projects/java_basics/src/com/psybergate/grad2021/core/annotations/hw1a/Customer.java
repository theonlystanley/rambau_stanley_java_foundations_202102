package com.psybergate.grad2021.core.annotations.hw1a;

public class Customer {

  private String customerNum;
  private String name;
  private String surname;
  private int dateOfBirth;
  private int age;

  public Customer(String customerNum, String name, String surname, int dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }
}
