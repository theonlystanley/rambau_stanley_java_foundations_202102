package com.psybergate.grad2021.core.annotations.hw3a;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseManager {

  private static final String ORG_POSTGRESQL_DRIVER = "org.postgresql.Driver";

  private static final String URL = "jdbc:postgresql://localhost:5432/tester";

  private static final String USERNAME = "postgres";

  private static final String PASSWORD = "admin";

  public static void main(String[] args) throws SQLException, ClassNotFoundException {
    generateDatabase();
  }

  public static void generateDatabase() throws SQLException, ClassNotFoundException {

    Class classObj = Customer.class;
    Field[] fields = classObj.getDeclaredFields();
    String tableName = classObj.getSimpleName();
    StringBuilder query = constructAQuery(fields, tableName);

    Connection conn = connectToDB();
    Statement statement = conn.createStatement();
    try {
      statement.execute(query.toString());
    } catch (SQLException e) {
      statement.execute("DROP TABLE " + tableName + ";");
      generateDatabase();
    }
    conn.close();
  }

  private static StringBuilder constructAQuery(Field[] fields, String tableName) {
    StringBuilder query = new StringBuilder("CREATE TABLE " + tableName + " \n(");
    for (Field field : fields) {
      Annotation[] annotations = field.getAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          DomainProperty currentAnnot = (DomainProperty) annotation;
          query.append(currentAnnot.name() + " ");
          query.append(field.getType().getSimpleName().equals("String") ? "varchar(50) " : "int ");
          query.append(currentAnnot.primaryKey() ? "Primary key " : "");
          query.append(currentAnnot.nullable() ? "not null " : "");
          query.append(currentAnnot.unique() ? "unique " : "");
          query.append(",\n");
        }
      }

    }
    query.setCharAt(query.length() - 2, ' ');
    query.append(")");
    return query;
  }

  public static Connection connectToDB() throws SQLException, ClassNotFoundException {
    Connection conn = null;
    try {
      Class.forName(ORG_POSTGRESQL_DRIVER);
      conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
    } catch (Exception e) {
      e.printStackTrace();
      System.err.println("Could not connect to DB.");
      throw e;
    }
    return conn;
  }

}
