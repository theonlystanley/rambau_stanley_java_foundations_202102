package com.psybergate.grad2021.core.annotations.hw3a;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class CustomerService {

  public static void main(String[] args) throws IllegalAccessException, SQLException, ClassNotFoundException {
    Customer cust = new Customer("002", "name2", "surname2", 19901209);
    saveCustomer(cust);
  }

  public static void saveCustomer(Customer customer) throws IllegalAccessException, SQLException, ClassNotFoundException {

    Class classObj = customer.getClass();
    Field[] fields = classObj.getDeclaredFields();
    String tableName = classObj.getSimpleName();

    StringBuilder query = constructInsertQuery(customer, fields, tableName);
    Connection conn = DatabaseManager.connectToDB();
    Statement statement = conn.createStatement();

    System.out.println(query);
    statement.execute(query.toString());
  }

  private static StringBuilder constructInsertQuery(Customer customer, Field[] fields, String tableName) throws IllegalAccessException {

    StringBuilder query = new StringBuilder("INSERT INTO " + tableName + "(");
    StringBuilder values = new StringBuilder("VALUES (");
    for (Field field : fields) {
      Annotation[] annotations = field.getAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          DomainProperty dp = (DomainProperty) annotation;
          query.append(dp.name() + ", ");

          Object value = field.get(customer);
          values.append((value.getClass().getSimpleName().equals("String") ? "'" + value + "'" : value) + ", ");
        }
      }
    }
    values.setCharAt(values.length() - 2, ' ');
    values.append(");");
    query.setCharAt(query.length() - 2, ' ');
    query.append(") \n" + values.toString());
    return query;
  }
}
