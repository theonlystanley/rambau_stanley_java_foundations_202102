package com.psybergate.grad2021.core.groupdemo;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class GroupLoader {

  public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {

    String value = PROPERTIES.getProperty("list");
    Class clazz = Class.forName(value);
    System.out.println("clazz.getSimpleName() = " + clazz.getSimpleName());
    List list = createTheList(clazz);


    for (Object o : list) {
      System.out.println("o = " + o);
    }
  }

  public static final Properties PROPERTIES = new Properties();

  static{
    ClassLoader classLoader = GroupLoader.class.getClassLoader();
    InputStream inputStream = classLoader.getResourceAsStream("app.properties");
    try {
      PROPERTIES.load(inputStream);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  private static List createTheList(Class clazz) throws IllegalAccessException, InstantiationException {

    List list = (List) clazz.newInstance();

    list.add("month1");
    list.add("month2");
    list.add("month3");
    list.add("month4");

    return list;
  }

}
