package com.psybergate.grad2021.core.generics.hw2;

import java.util.ArrayList;
import java.util.List;

public class Utility {

  public static void main(String[] args) {

    List<Integer> list = new ArrayList<>();
    list.add(1000);
    list.add(1001);
    list.add(10);
    list.add(8800);
    list.add(3000);
    list.add(400);
    list = GreaterThan1000.greaterThan1000(list);
    printList(list);

    List<String> list1 = new ArrayList<>();
//    list1 = GreaterThan1000.greaterThan1000(list1);
  }

  private static void printList(List<Integer> list) {
    for (Integer integer : list) {
      System.out.println(integer);
    }
  }
}
