package com.psybergate.grad2021.core.generics.hw1c;

import java.util.ArrayList;
import java.util.List;

public class ReverseListAfterErasure {

  public static void main(String[] args) {

    List<String> list = new ArrayList<>();
    list.add("mazda");
    list.add("audi");
    list.add("toyota");
    list.add("kia");
    list.add("benz");
    printList(list);
    list = reverse(list);
    System.out.println("\nReversedList: ");
    printList(list);
  }

  private static void printList(List list) {
    for (Object s : list) {
      System.out.println(s);
    }
  }

  public static List<String> reverse(List list) {
    List reversedlist = new ArrayList();

    for (int i = 0; i < list.size(); i++) {
      StringBuilder stringBuilder = new StringBuilder((String)list.get(i)).reverse();
      reversedlist.add(stringBuilder.toString());
    }
    return reversedlist;
  }
}
