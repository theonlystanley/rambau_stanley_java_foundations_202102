package com.psybergate.grad2021.core.generics.hw3b;

import com.psybergate.grad2021.core.generics.hw3a.Account;

import java.util.Arrays;
import java.util.Collection;

public class Utility {

  public static void main(String[] args) {

    Collection<Integer> coll = Arrays.asList(2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
    Collection<String> coll2 = Arrays.asList("change", "Cat", "grace");
    Collection<Account> coll3 = Arrays.asList(new Account(1200),new Account(-1),new Account(90));

    System.out.println("Even Numbers \t\t= " + counter(coll,new Even()));
    System.out.println("Odd Numbers \t\t= " + counter(coll,new Odd()));
    System.out.println("Prime Numbers \t\t= " + counter(coll,new Prime()));
    System.out.println("Start with Letter C \t= " + counter(coll2,new StartWithC()));
    System.out.println("Balance less than 0 \t= " + counter(coll3,new Balance()));
  }

  public static <T> int counter(Collection<T> coll, Checker<T> checker) {
    int count = 0;
    for (T t : coll) {
      if (checker.check(t)) {
        count++;
      }
    }
    return count;
  }
}
