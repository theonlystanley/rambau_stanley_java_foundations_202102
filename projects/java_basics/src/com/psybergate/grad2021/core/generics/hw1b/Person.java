package com.psybergate.grad2021.core.generics.hw1b;

public class Person {

  private String name;

  public Person(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
