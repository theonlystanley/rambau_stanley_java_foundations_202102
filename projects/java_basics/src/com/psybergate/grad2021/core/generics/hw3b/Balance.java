package com.psybergate.grad2021.core.generics.hw3b;

import com.psybergate.grad2021.core.generics.hw3a.Account;

public class Balance implements Checker<Account> {

  @Override
  public boolean check(Account account) {
    return account.getBalance() < 0;
  }
}
