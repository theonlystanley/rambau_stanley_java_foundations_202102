package com.psybergate.grad2021.core.generics.hw1b;

import java.util.ArrayList;
import java.util.List;

public class TeamAfterErasure{

  private String name;
  private Person teamLeader;
  private List<Person> teamMembers = new ArrayList<>();

  public TeamAfterErasure(String name, Person teamLeader) {
    this.name = name;
    this.teamLeader = teamLeader;
  }

  public void addMember(Person t){
    this.teamMembers.add(t);
  }

  public List<Person> getTeamMembers() {
    return teamMembers;
  }

  public Person getTeamLeader() {
    return teamLeader;
  }
}