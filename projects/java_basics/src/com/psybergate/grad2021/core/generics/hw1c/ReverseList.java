package com.psybergate.grad2021.core.generics.hw1c;

import java.util.ArrayList;
import java.util.List;

public class ReverseList {

  public static void main(String[] args) {

    List<String> list = new ArrayList<>();
    list.add("mazda");
    list.add("audi");
    list.add("toyota");
    list.add("kia");
    list.add("benz");
    printList(list);
    list = reverse(list);
    System.out.println("\nReversedList: ");
    printList(list);
  }

  private static void printList(List<String> list) {
    for (String s : list) {
      System.out.println(s);
    }
  }

  public static List<String> reverse(List<String> list) {
    List<String> reversedlist = new ArrayList<>();

    for (int i = 0; i < list.size(); i++) {
      StringBuilder stringBuilder = new StringBuilder(list.get(i)).reverse();
      reversedlist.add(stringBuilder.toString());
    }
    return reversedlist;
  }
}
