package com.psybergate.grad2021.core.generics.hw3b;

public class Prime implements Checker<Integer>{

  @Override
  public boolean check(Integer element) {
    int mid = element/2;
    boolean isPrime = true;
    for (int i = 2; i <= mid; i++) {
      if (element % i == 0) {
        isPrime = false;
        break;
      }
    }
    return isPrime;
  }
}
