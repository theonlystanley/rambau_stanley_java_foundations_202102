package com.psybergate.grad2021.core.generics.hw4a;

public class SuperClass <T>{

  private T data;

  public void setData(T data) {
    this.data = data;
    throw new RuntimeException();
  }
}
