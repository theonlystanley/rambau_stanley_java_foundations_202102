package com.psybergate.grad2021.core.generics.hw3a;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MyMethods {

  public static void main(String[] args) {

    List<Integer> numbers = new ArrayList<>();
    int num = 1000;
    for (int i = 1; i <= num; i++) {
      numbers.add(i);
    }

    System.out.println("evenNumbers(numbers) = " + evenNumbers(numbers));
    System.out.println("oddNumbers(numbers) = " + oddNumbers(numbers));
    System.out.println("primeNumbers(numbers) = " + primeNumbers(numbers));


  }

  public static int evenNumbers(Collection<Integer> coll) {
    int count = 0;
    for (Integer integer : coll) {
      int i = integer;
      if (i % 2 == 0) {
        count++;
      }
    }
    return count;
  }

  public static int oddNumbers(Collection<Integer> coll) {
    int count = 0;
    for (Integer integer : coll) {
      int i = integer;
      if (i % 2 != 0) {
        count++;
      }
    }
    return count;
  }

  public static int primeNumbers(Collection<Integer> coll) {
    int count = 0;
    for (Integer integer : coll) {
      int num = integer;
      int mid = num/2;
      boolean isPrime = true;
      if (num == 0 || num == 1) {
        count++;
      } else{
        for (int i = 2; i <= mid; i++) {
          if (num % i == 0) {
            isPrime = false;
            break;
          }
        }
        if (isPrime) {
          count++;
        }
      }
    }
    return count;
  }

  public static int startWithC(Collection<String> coll) {
    int count = 0;
    for (String s : coll) {
      if (s.charAt(0) == 'c' || s.charAt(0) == 'C') {
        count++;
      }
    }

    return count;
  }

  public static int balanceLessThan0(Collection<Account> coll) {
    int count = 0;
    for (Account account : coll) {
      if (account.getBalance() < 0) {
        count++;
      }
    }
    return count;
  }
}
