package com.psybergate.grad2021.core.generics.hw3b;

public class StartWithC implements Checker<String>{

  @Override
  public boolean check(String element) {
    return element.charAt(0) == 'c' || element.charAt(0) == 'C';
  }
}
