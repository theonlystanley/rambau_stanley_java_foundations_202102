package com.psybergate.grad2021.core.generics.hw1b;

import java.util.ArrayList;
import java.util.List;

public class Team <T extends Person>{

  private String name;
  private T teamLeader;
  private List<T> teamMembers = new ArrayList<>();

  public Team(String name, T teamLeader) {
    this.name = name;
    this.teamLeader = teamLeader;
  }

  public void addMember(T t){
    this.teamMembers.add(t);
  }

  public List<T> getTeamMembers() {
    return teamMembers;
  }

  public T getTeamLeader() {
    return teamLeader;
  }
}
