package com.psybergate.grad2021.core.generics.hw2;

import java.util.ArrayList;
import java.util.List;

public class GreaterThan1000FIle {

  public static List<Integer> greaterThan1000(List<Integer> list) {

    List<Integer> newList = new ArrayList<>();
    for (Integer integer : list) {
      if (integer.intValue() > 1000) {
        newList.add(integer);
      }
    }
    return newList;
  }
}
