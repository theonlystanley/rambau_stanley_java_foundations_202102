package com.psybergate.grad2021.core.generics.hw1bv2;

import java.util.ArrayList;
import java.util.List;

/**
 * This is how the runtime version of the Team class looks like - although this may not be 100% exact (there may be 
 * some additional internal implementation details), one can confirm this (at least conceptually) via
 * running javap on the <code>Team</code> class
 */
//@SuppressWarnings({"unchecked", "rawtypes"})
@SuppressWarnings("rawtypes")
//@SuppressWarnings("unchecked")
public class TeamWithoutGenerics {

  private static final int MAX_TEAM_SIZE = 15;

  private List people = new ArrayList();

  public static int maxTeamSize() {
    return MAX_TEAM_SIZE;
  }

  public void addPerson(Person t) {
    people.add(t);
  }

  public Person getPerson(int position) {
    return (Person) people.get(position);
  }

  public int getNumofPersons() {
    return people.size();
  }

  public Person[] asArray(Person[] e) {
    for (int i = 0; i < people.size(); i++) {
      e[i] = (Person) getPerson(i);
    }
    return e;
  }

  public static void main(String[] args) {
    TeamWithoutGenerics team = new TeamWithoutGenerics();
    team.addPerson(new Student("Chris", 40));
    team.addPerson(new Student("Colin", 35));
    for (int i = 0; i < team.getNumofPersons(); i++) {
      System.out.println(i + ": " + team.getPerson(i).getName());
      System.out.println(i + " yearregistered: " + ((Student) team.getPerson(i)).getYearRegistered());
    }

    Student[] students = (Student[]) team.asArray(new Student[2]);
    for (Student student : students) {
      System.out.println("StudentArray: " + student.getName());
    }
  }

}
