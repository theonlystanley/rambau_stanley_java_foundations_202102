package com.psybergate.grad2021.core.generics.hw3b;

public class Even implements Checker<Integer>{
  @Override
  public boolean check(Integer element) {
    return element % 2 == 0;
  }
}
