package com.psybergate.grad2021.core.generics.hw3b;

public interface Checker <T> {

  boolean check(T element);
}
