package com.psybergate.grad2021.core.generics.hw4a;

public class ChildClass extends SuperClass<Integer> {

  public static void main(String[] args) {
    SuperClass c = new ChildClass();
    c.setData(56);
  }

  @Override
  public void setData(Integer data) {
    super.setData(data);
  }
}
