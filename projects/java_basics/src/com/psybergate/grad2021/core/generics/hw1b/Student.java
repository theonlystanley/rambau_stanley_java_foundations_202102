package com.psybergate.grad2021.core.generics.hw1b;

public class Student extends Person{

  private String studentNum;

  public Student(String name, String studentNum) {
    super(name);
    this.studentNum = studentNum;
  }
}
