package com.psybergate.grad2021.core.exceptions.hw4;

public class Account {

  public String accountNum; // Assign a random number

  public String email;

  public String password;

  // Fields to be inserted: User it belongs to, balance, signup date, account type

  public Account(String email, String password) {
    this.accountNum = "01"; // to be changed
    this.email = email;
    this.password = password;
  }

  public String getAccountNum() {
    return accountNum;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;

    if (o == null) return false;

    if (getClass() != o.getClass()) return false;

    Account account = (Account) o;
    return email.equals(account.email) && password.equals(account.password);
  }

}
