package com.psybergate.grad2021.core.exceptions.ce1;

public class SquashingException {

  public static void main(String[] args) {

    try {
      doSomething1();
    } catch (IllegalArgumentException e) {
      e.printStackTrace(); // This is just printing out the stack track and method continues.
    }
    doSomethingAfter();
  }

  private static void doSomethingAfter() {
    System.out.println("Executing");
  }

  private static void doSomething1() throws IllegalArgumentException {
    throw new IllegalArgumentException();
  }
}
