package com.psybergate.grad2021.core.exceptions.hw4_v2;

public class InvalidAmountException extends Exception {

  public InvalidAmountException() {
  }

  public InvalidAmountException(String message) {
    super(message);
  }
}
