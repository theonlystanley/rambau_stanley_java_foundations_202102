package com.psybergate.grad2021.core.exceptions.hw4;

public class AccountValidator {

  public static String validateLoginDetails(String email, String password) throws Exception {
    // Access the database
    // if fails propagates an exception
    if (email == null || password == null) {
      throw new IllegalArgumentException("Email or Password is null");
    }

    AccountDB db = new AccountDB();
    if (db.getAccounts().size() < 1) {
      throw new Exception("Couldn't connect to the database");
    }
    for (Account account : db.getAccounts()) {
      if (account.equals(new Account(email, password))) {
        return account.getAccountNum();
      }
    }
    return null;
  }
}
