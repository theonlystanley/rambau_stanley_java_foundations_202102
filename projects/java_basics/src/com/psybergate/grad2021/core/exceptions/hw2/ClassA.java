package com.psybergate.grad2021.core.exceptions.hw2;

import java.sql.SQLException;

public class ClassA {

  public static void methodA() throws ApplicationException {
    try {
      methodB();
    } catch (SQLException ex) {
      throw new ApplicationException(ex);
    }
  }

  private static void methodB() throws SQLException {
    // calls the database
    throw new SQLException("Database connection failed");
  }
}
