package com.psybergate.grad2021.core.exceptions.ce2;

public class ClassB {

  public void method1() throws CustomCheckedException {
    throw new CustomCheckedException();
  }

  public void method2() {
    throw new CustomError();
  }

  public void method3() {
    throw new CustomRuntimeException();
  }

  public void method4() throws CustomThrowable {
    throw new CustomThrowable();
  }
}
