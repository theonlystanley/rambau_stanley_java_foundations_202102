package com.psybergate.grad2021.core.exceptions.hw4_v2;

public class AccountDoesNotExistException extends Exception {
  public AccountDoesNotExistException(String message) {
    super(message);
  }
}
