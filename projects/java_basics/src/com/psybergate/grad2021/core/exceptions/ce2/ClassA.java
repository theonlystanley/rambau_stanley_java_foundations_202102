package com.psybergate.grad2021.core.exceptions.ce2;

public class ClassA {

  public ClassB classB = new ClassB();

  public void method1() throws CustomCheckedException {
    classB.method1();
  }

  public void method2() {
    classB.method2();
  }

  public void method3() {
    classB.method3();
  }

  public void method4() throws CustomThrowable {
    classB.method4();
  }

}
