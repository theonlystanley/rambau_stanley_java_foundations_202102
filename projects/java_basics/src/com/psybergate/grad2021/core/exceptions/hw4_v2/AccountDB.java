package com.psybergate.grad2021.core.exceptions.hw4_v2;

import java.util.ArrayList;
import java.util.List;

public class AccountDB {

  private List<Account> accounts = new ArrayList<>();

  public AccountDB() {
    generateAccounts();
  }

  private void generateAccounts() {
    accounts.add(new Account("01", 1000));
    accounts.add(new Account("02", 5000));
    accounts.add(new Account("03", 2000));
    accounts.add(new Account("04", 1300));
  }

  public List<Account> getAccounts() {
    return accounts;
  }
}
