package com.psybergate.grad2021.core.exceptions.hw4;

import java.util.Scanner;

public class ClientRunner {

  public static void main(String[] args) {

    boolean isLoggedIn = false;
    String accountNum = "";
    Scanner input = new Scanner(System.in);

    System.out.println("");
    System.out.println("Welcome Please Login");

    while (!isLoggedIn) {
      System.out.println("Enter your Email: ");
      String email = input.next();
      System.out.println("Enter your Password: ");
      String password = input.next();
      try {
        accountNum = AccountController.getLoginInputs(email, password);
        isLoggedIn = true;
        System.out.println("Successfully Logged In.");
      } catch (IllegalArgumentException | UserNotFoundException e) {
        System.err.println(e.getMessage());
      } catch (Exception e) {
        System.err.println(e.getMessage() + " Try again later.");
        break;
      }
    }

    while (isLoggedIn) {
      System.out.println("");
      System.out.println("Menu");
      System.out.println("1. Check balance.");
      System.out.println("2. View account details");
      System.out.println("2. Change account details.");
      input.next();
    }

  }
}
