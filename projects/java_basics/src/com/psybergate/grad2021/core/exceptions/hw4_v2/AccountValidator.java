package com.psybergate.grad2021.core.exceptions.hw4_v2;

public class AccountValidator {

  public static void validateAmount(double amount) throws InvalidAmountException {
    if(amount <= 50){
      throw new InvalidAmountException("Amount cannot be less than 50.");
    }
  }

  public static void validateAccountNum(String accountNum){
    if(accountNum == null){
      throw new IllegalArgumentException("Account Number is Null.");
    }
  }
}
