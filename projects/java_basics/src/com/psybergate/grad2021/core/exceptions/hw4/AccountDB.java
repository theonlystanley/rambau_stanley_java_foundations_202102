package com.psybergate.grad2021.core.exceptions.hw4;

import java.util.ArrayList;
import java.util.List;

public class AccountDB {

  private List<Account> accounts = new ArrayList<>();

  public AccountDB() {
    generateAccounts();
  }

  private void generateAccounts() {
    accounts.add(new Account("abc@g.com", "1234"));
    accounts.add(new Account("def@g.com", "5678"));
    accounts.add(new Account("ghi@g.com", "90"));
  }

  public List<Account> getAccounts() {
    return accounts;
  }
}
