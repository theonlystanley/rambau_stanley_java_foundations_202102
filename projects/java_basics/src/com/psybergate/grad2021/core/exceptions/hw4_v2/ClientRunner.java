package com.psybergate.grad2021.core.exceptions.hw4_v2;

import java.util.Scanner;

public class ClientRunner {

  public static void main(String[] args) throws Exception {

    int menuChoice = 0;
    Scanner scan = new Scanner(System.in);
    String accountNum;
    double amount;
    System.out.println("Welcome");
    System.out.println("Select an options: \n1. Deposit \n2. Withdraw \nChoice:");

    while (true){
      menuChoice = scan.nextInt();

      switch (menuChoice) {
        case 1:
          System.out.println("Enter accountNum:");
          accountNum = scan.next();
          System.out.println("Enter amount:");
          amount = scan.nextInt();
          AccountController.depositInputs(accountNum, amount);
          break;
        case 2:
          System.out.println("Enter accountNum:");
          accountNum = scan.next();
          System.out.println("Enter amount:");
          amount = scan.nextInt();
          AccountController.withdrawInputs(accountNum, amount);
          break;
        default:
          System.out.println("Invalid selection");
      }
    }
  }
}
