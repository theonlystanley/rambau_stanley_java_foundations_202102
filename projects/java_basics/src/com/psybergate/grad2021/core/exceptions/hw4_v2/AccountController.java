package com.psybergate.grad2021.core.exceptions.hw4_v2;

public class AccountController {

  public static void depositInputs(String accountNum, double amount) throws Exception{
    AccountService.deposit(accountNum, amount);
  }

  public static void withdrawInputs(String accountNum, double amount) throws Exception {
    AccountService.withdraw(accountNum, amount);
  }
}
