package com.psybergate.grad2021.core.exceptions.hw4;

public class AccountService {

  public static String Login(String email, String password) throws Exception {
    try {
      String AccountNum = AccountValidator.validateLoginDetails(email, password);
      if (AccountNum == null) {
        throw new UserNotFoundException("Invalid details.");
      }
      return AccountNum;
    } catch (Exception e) {
      throw new Exception(e);
    }
  }
}
