package com.psybergate.grad2021.core.exceptions.hw2;

public class ApplicationException extends RuntimeException {

  public ApplicationException(Throwable cause) {
    super(cause);
  }
}
