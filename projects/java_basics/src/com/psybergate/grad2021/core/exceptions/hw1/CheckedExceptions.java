package com.psybergate.grad2021.core.exceptions.hw1;

public class CheckedExceptions {

  public static void main(String[] args) throws Exception {
    method1();
  }

  // All methods needs to indicate this throw exception
  public static void method1() throws Exception {
    method2();
  }

  public static void method2() throws Exception {
    method3();
  }

  public static void method3() throws Exception {
    method4();
  }

  public static void method4() throws Exception {
    method5();
  }

  public static void method5() throws Exception {
    throw new Exception();
  }
}
