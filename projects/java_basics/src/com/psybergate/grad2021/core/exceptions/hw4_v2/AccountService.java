package com.psybergate.grad2021.core.exceptions.hw4_v2;

public class AccountService {

  private static AccountDB accountDB = new AccountDB();

  public static Account deposit(String accountNum, double amount) throws InvalidAmountException, AccountDoesNotExistException {

    AccountValidator.validateAccountNum(accountNum);
    AccountValidator.validateAmount(amount);
    for (Account account : accountDB.getAccounts()) {
      if (account.getAccountNum().equals(accountNum)){
        if(account.getBalance() < amount){
          throw new InvalidAmountException("Amount is greater than available balance");
        }else{
          double balance = account.getBalance() - amount;
          account.setBalance(balance);
          return  account;
        }
      }
    }
    throw new AccountDoesNotExistException("Account does not exist");
  }

  public static void withdraw(String accountNum, double amount) {
  }
}
