package com.psybergate.grad2021.core.exceptions.hw4;

public class UserNotFoundException extends Exception {

  public UserNotFoundException(String message) {
    super(message);
  }
}
