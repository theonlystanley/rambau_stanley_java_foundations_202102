package com.psybergate.grad2021.core.exceptions.hw4_v2;

public class Account {

  private String accountNum; // Assign a random number

  private double balance;

  public Account(String accountNum, double balance) {
    this.accountNum = accountNum;
    this.balance = balance;
  }

  public String getAccountNum() {
    return accountNum;
  }

  public double getBalance() {
    return balance;
  }

  public void setBalance(double balance) {
    this.balance = balance;
  }
}
