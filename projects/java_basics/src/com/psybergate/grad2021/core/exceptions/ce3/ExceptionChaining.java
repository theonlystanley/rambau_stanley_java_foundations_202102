package com.psybergate.grad2021.core.exceptions.ce3;

public class ExceptionChaining {

  public static void main(String[] args) throws Exception {
    try {
      method1();
    } catch (Throwable e) {
      e.printStackTrace(); // Squashing the exceptions
    }
  }

  public static void method1() throws Exception {
    try {
      method2();
    } catch (Exception e) {
      throw new Exception(e);
    }
  }

  public static void method2() {
    try {
      method3();
    } catch (IllegalArgumentException e) {
      throw new RuntimeException(e);
    }
  }

  public static void method3() {
    throw new IllegalArgumentException("Invalid amount passed.");
  }
}
