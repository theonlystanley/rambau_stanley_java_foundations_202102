package com.psybergate.grad2021.core.collections.groupdemo;

public class Product {

  private int prodNum;

  public int getProdNum() {
    return prodNum;
  }

  public void setProdNum(int prodNum) {
    this.prodNum = prodNum;
  }
}
