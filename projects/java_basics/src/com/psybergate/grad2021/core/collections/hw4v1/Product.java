package com.psybergate.grad2021.core.collections.hw4v1;

public class Product {

  private int prodNum;

  public int getProdNum() {
    return prodNum;
  }

  public void setProdNum(int prodNum) {
    this.prodNum = prodNum;
  }
}
