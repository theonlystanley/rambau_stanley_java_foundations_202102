package com.psybergate.grad2021.core.collections.hw4a.singlylinkedlist;

import java.util.Iterator;

public class MyLinkedList extends AbstractSinglyLinkedLIst {

  private Node head;

  /**
   * represent the last node in my list
   */
  private Node last;

  private int size;

  public MyLinkedList() {
    this.head = new Node(null, null);
    this.last = head;
  }

  @Override
  public boolean add(Object o) {
    Node newNode = new Node(o, null);
    last.setNext(newNode);
    last = newNode;
    size++;
    return true;
  }

  @Override
  public Object get(int index) {
    if (isOutOfBound(index)) {
      throw new IllegalArgumentException("Invalid index passed.");
    }
    int position = 0;

    for (Iterator iterator = this.iterator(); iterator.hasNext(); ) {
      Object currentObject = iterator.next();
      if (position == index) {
        return currentObject;
      }
      position++;
    }
    return null;
  }

  private boolean isOutOfBound(int index) {
    return index > size - 1;
  }

  @Override
  public Object remove(int index) {
    // Check for negatives too.
    if (isEmpty() || isOutOfBound(index)) {
      throw new IndexOutOfBoundsException("Invalid index passed.");
    }

    int position = -1;
    for (Node currentNode = head;currentNode != null;position++,currentNode = currentNode.getNext()) {
      if (position == index - 1) {
        Node temp = currentNode.getNext();
        Object data = temp.getData();
        currentNode.setNext(currentNode.getNext().getNext());
        temp.setNext(null);
        return data;
      }
    }
    return null;
  }

  @Override
  public void add(int index, Object element) {
    if (isOutOfBound(index)) {
      throw new IndexOutOfBoundsException("Invalid index passed.");
    }
    int position = -1;
    for (Node currentNode = head; currentNode != null; position++, currentNode = currentNode.getNext()) {
      if (position == index - 1) {
        Node newNode = new Node(element, currentNode.getNext());
        currentNode.setNext(newNode);
        return;
      }
    }
  }

  @Override
  public Object set(int index, Object element) {
    if (isEmpty() || index >= size) {
      throw new IndexOutOfBoundsException("Cannot set on an empty list.");
    }

    int position = -1;
    for (Node currentNode = head; currentNode != null; position++, currentNode = currentNode.getNext()) {
      if (position == index - 1) {
        Node temp = currentNode.getNext();
        Object data = temp.getData();
        Node newNode = new Node(element, temp.getNext());
        currentNode.setNext(newNode);
        temp.setNext(null);
        return data;
      }
    }
    return null;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return head.getNext() == null;
  }

  @Override
  public boolean contains(Object o) {
    if (o == null) {
      throw new NullPointerException("Null object was passed.");
    }
    for (Iterator iterator = this.iterator(); iterator.hasNext(); ) {
      Object currentObject = iterator.next();
      if (o.equals(currentObject)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Iterator iterator() {
    return new MyListIterator(head);
  }

}
