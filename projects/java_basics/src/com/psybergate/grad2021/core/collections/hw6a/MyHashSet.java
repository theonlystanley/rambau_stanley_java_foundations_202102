package com.psybergate.grad2021.core.collections.hw6a;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class MyHashSet implements Set {

  private Node[] buckets;

  private int size;

  public MyHashSet(int capacity) {

    buckets = new Node[capacity];
    size = 0;
  }

  public boolean add(Object element) {

    int index = hashFunction(element.hashCode());
    Node current = buckets[index];

    while (current != null) {
      if (current.element.equals(element)) {
        return false;
      }
      current = current.next;
    }

    Node node = new Node();
    node.element = element;
    node.next = buckets[index];
    buckets[index] = node;
    size++;
    return true;
  }

  public boolean contains(Object element) {

    int index = hashFunction(element.hashCode());
    Node current = buckets[index];

    while (current != null) {
      if (current.element.equals(element)) {
        return true;
      }
      current = current.next;
    }
    return false;
  }

  public boolean remove(Object element) {

    int index = hashFunction(element.hashCode());
    Node current = buckets[index];
    Node previous = null;

    while (current != null) {
      if (current.element.equals(element)) {

        if (previous == null) {
          buckets[index] = current.next;
        } else {
          previous.next = current.next;
        }
        size--;
        return true;
      }

      previous = current;
      current = current.next;
    }
    return false;
  }

  public int size() {
    return size;
  }

  private int hashFunction(int hashCode) {

    int index = hashCode;
    if (index < 0) {
      index = -index;
    }
    return index % buckets.length;
  }

  @Override
  public String toString() {

    Node currentNode = null;
    StringBuffer sb = new StringBuffer();

    for (int index = 0; index < buckets.length; index++) {
      if (buckets[index] != null) {
        currentNode = buckets[index];
        sb.append("[" + index + "]");
        sb.append(" " + currentNode.element.toString());
        while (currentNode.next != null) {
          currentNode = currentNode.next;
          sb.append(" -> " + currentNode.element.toString());
        }
        sb.append('\n');
      }
    }

    return sb.toString();
  }

  public Iterator iterator() {
    return null;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public Object[] toArray(Object[] a) {
    return new Object[0];
  }

  @Override
  public boolean containsAll(Collection c) {
    return false;
  }

  @Override
  public boolean addAll(Collection c) {
    return false;
  }

  @Override
  public boolean retainAll(Collection c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection c) {
    return false;
  }

  @Override
  public void clear() {

  }

  @Override
  public boolean equals(Object o) {
    return false;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  private class MyIterator implements Iterator{

    private Node currentNode;
    private int currentIndex = 0;

    @Override
    public boolean hasNext() {
      return false;
    }

    @Override
    public Object next() {
      currentNode = buckets[currentIndex];
      if (currentNode != null){

      }

      return null;
    }
  }

  private static class Node {
    Object element;
    Node next;
  }
}
















