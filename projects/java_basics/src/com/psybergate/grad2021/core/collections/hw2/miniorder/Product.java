package com.psybergate.grad2021.core.collections.hw2.miniorder;

public class Product {

  private String productName;

  private double price;

  private String supplier;

  public Product(String productName, double price, String supplier) {
    this.productName = productName;
    this.price = price;
    this.supplier = supplier;
  }

  public String getProductName() {
    return productName;
  }

  public double getPrice() {
    return price;
  }

  public String getSupplier() {
    return supplier;
  }

  @Override
  public String toString() {
    return "productName='" + productName + '\'' +
            ", price=" + price +
            ", supplier='" + supplier + '\'';
  }
}
