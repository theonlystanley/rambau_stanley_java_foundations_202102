package com.psybergate.grad2021.core.collections.hw4a.singlylinkedlist;

import java.util.List;

public class SinglyLinkedListUtility {

  public static void main(String[] args) {

    List list = new MyLinkedList();

    list.add("ab1");
    list.add("ab2");
    list.add("ab3");
    list.add("ab4");
    print(list);
    System.out.println("list.contains(\"ab1\") = " + list.contains("ab1"));
    System.out.println("list.size() = " + list.size());
    //print(list);
    int index = 2;
    list.set(index,"new");
    print(list);
    System.out.println("list.get(" + index + ") = " + list.get(index));
    System.out.println("list.remove("+index+") = " + list.remove(index));
    print(list);
  }

  private static void print(List list) {
    for (Object o : list) {
      System.out.println("o = " + o);
    }
  }
}
