package com.psybergate.grad2021.core.collections.hw5a;

import java.util.Collection;

public interface Stack extends Collection {

  void push(Object object);

  Object pop();

  Object get(int position);
}
