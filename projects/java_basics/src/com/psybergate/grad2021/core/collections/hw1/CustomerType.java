package com.psybergate.grad2021.core.collections.hw1;

public enum CustomerType {

  LOCAL, INTERNATIONAL
}
