package com.psybergate.grad2021.core.collections.hw4a.doublylinkedlist;

import java.util.Iterator;

public class MyLinkedList extends AbstractDoublyLinkedList {

  private Node head;

  private Node tail;

  private int size = 0;

  public MyLinkedList() {
    this.head = new Node(null, null, null);
    this.tail = new Node(null, null, head);
    this.head.setNext(tail);
  }

  @Override
  public boolean add(Object o) {
    Node newNode = new Node(o, tail, tail.getPrevious());
    tail.getPrevious().setNext(newNode);
    tail.setPrevious(newNode);
    size++;
    return true;
  }

  @Override
  public Object get(int index) {
    if (isOutOfBound(index)) {
      throw new IndexOutOfBoundsException("Invalid index passed.");
    }
    int position = 0;

    for (Iterator iterator = this.iterator(); iterator.hasNext(); ) {
      Object data = iterator.next();
      if (position == index) {
        return data;
      }
      position++;
    }

    return null;
  }

  @Override
  public boolean remove(Object o) {
    return false;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return head.getNext() == tail;
  }

  @Override
  public boolean contains(Object o) {
    if (o == null) {
      throw new NullPointerException("Null object was passed.");
    }
    for (Iterator iterator = this.iterator(); iterator.hasNext(); ) {
      Object currentObject = iterator.next();
      if (o.equals(currentObject)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Iterator iterator() {
    return new MyListIterator(head);
  }

  @Override
  public Object set(int index, Object element) {
    if (isEmpty() || index >= size) {
      throw new IndexOutOfBoundsException("Cannot set on an empty list.");
    }

    int position = 0;
    for (Node currentNode = head.getNext(); currentNode != tail; position++, currentNode = currentNode.getNext()) {
      if (position == index) {
        Node temp = currentNode;
        Object data = temp.getData();
        Node newNode = new Node(element, temp.getNext(), temp.getPrevious());
        temp.getPrevious().setNext(newNode);
        temp.getNext().setPrevious(newNode);
        temp.setNext(null);
        temp.setPrevious(null);
        return data;
      }
    }
    return null;
  }

  @Override
  public void add(int index, Object element) {
    if (isOutOfBound(index)) {
      throw new IndexOutOfBoundsException("Invalid index passed.");
    }
    int position = 0;
    for (Node currentNode = head.getNext(); currentNode != tail; position++, currentNode = currentNode.getNext()) {
      if (position == index) {
        Node newNode = new Node(element, currentNode, currentNode.getPrevious());
        currentNode.setPrevious(newNode);
        currentNode.getPrevious().setNext(newNode);
        return;
      }
    }
  }

  @Override
  public Object remove(int index) {
    if (isEmpty() || isOutOfBound(index)) {
      throw new IndexOutOfBoundsException("Invalid index passed.");
    }

    int position = 0;
    for (Node currentNode = head.getNext(); currentNode != tail; position++, currentNode = currentNode.getNext()) {
      if (position == index) {
        Node temp = currentNode;
        Object data = temp.getData();
        temp.getNext().setPrevious(temp.getPrevious());
        temp.getPrevious().setNext(temp.getNext());
        temp.setNext(null);
        temp.setPrevious(null);
        return data;
      }
    }
    return null;
  }

  private boolean isOutOfBound(int index) {
    return index > size - 1;
  }

}
