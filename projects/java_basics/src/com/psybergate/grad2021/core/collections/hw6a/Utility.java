package com.psybergate.grad2021.core.collections.hw6a;

public class Utility {

  public static void main(String[] args) {

    MyHashSet set = new MyHashSet(50);

    set.add("ac");
    set.add("zx");
    set.add("lirtr");
    set.add("ere");
    set.add("dsdss");
    set.add("dsdss");
    set.add("dsdss");
    set.add("ty");
    System.out.println(set.toString());
  }
}
