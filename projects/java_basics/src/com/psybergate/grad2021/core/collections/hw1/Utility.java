package com.psybergate.grad2021.core.collections.hw1;

import java.util.SortedSet;
import java.util.TreeSet;

import static com.psybergate.grad2021.core.collections.hw1.ReverseOrderComparator.REVERSE_ORDER_COMPARATOR;

public class Utility {

  public static void main(String[] args) {

    Customer c1 = new Customer(1, "bill", "gate", CustomerType.LOCAL);
    Customer c2 = new Customer(2, "john", "smith", CustomerType.LOCAL);
    Customer c3 = new Customer(3, "stan", "rams", CustomerType.LOCAL);
    Customer c4 = new Customer(4, "joseph", "coulter", CustomerType.LOCAL);
    Customer c5 = new Customer(4, "jose", "james", CustomerType.LOCAL);

    SortedSet sortedSet = new TreeSet<>(REVERSE_ORDER_COMPARATOR);
    sortedSet.add(c1);
    sortedSet.add(c4);
    sortedSet.add(c2);
    sortedSet.add(c3);
    printSet(sortedSet);
  }

  private static void printSet(SortedSet sortedSet) {
    for (Object obj : sortedSet) {
      System.out.println("obj = " + obj);
    }
  }
}