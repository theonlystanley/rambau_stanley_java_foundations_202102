package com.psybergate.grad2021.core.collections.hw4v1.doublylinkedlist;

import com.psybergate.grad2021.core.collections.hw4v1.CustomList;
import com.psybergate.grad2021.core.collections.hw4v1.Node;

import java.util.Iterator;

public class CustomDoublyLinkedList implements CustomList, Iterable {

  private Node head = null;

  private Node tail = null;

  private int size = 0;

  public CustomDoublyLinkedList() {
    this.tail = new Node(null,null,null);
    this.head = new Node(null,tail,null);
  }

  @Override
  public boolean add(Object data) {
    Node newNode = new Node(data, head.getNext(), head);
    head.setNext(newNode);
    newNode.getNext().setPrev(newNode);
    size++;
    return true;
  }

  @Override
  public boolean addFirst() {
    return false;
  }

  @Override
  public boolean addLast() {
    return false;
  }

  @Override
  public boolean isEmpty(){
    return size > 0;
  }

  @Override
  public boolean remove() {
    return false;
  }

  public int getSize() {
    return size;
  }

  @Override
  public String toString() {
    String results = head.toString() + " <-> ";
    Node currentNode = head.getNext();

    while (currentNode.getNext()!= tail){
      results += currentNode.toString() + " <-> ";
      currentNode = currentNode.getNext();
    }

    results += tail.toString();
    return results;
  }

  @Override
  public Iterator iterator() {
    return new MyListIterator();
  }

  class MyListIterator implements Iterator {

    private Node cursor;

    public MyListIterator(){
      cursor = head.getNext();
    }

    @Override
    public boolean hasNext() {
      return cursor.getNext() != null;
    }

    @Override
    public Object next() {
      Object data = cursor.getData();
      cursor = cursor.getNext();
      return data;
    }
  }
}
