package com.psybergate.grad2021.core.collections.hw5a;

import java.util.Comparator;

public class AscendingOrderComparator implements Comparator {

  public static final AscendingOrderComparator ASCENDING_ORDER_COMPARATOR = new AscendingOrderComparator();

  @Override
  public int compare(Object o1, Object o2) {
    String s1 = (String) o1;
    String s2 = (String) o2;
    return -1 * s1.compareTo(s2);
  }
}
