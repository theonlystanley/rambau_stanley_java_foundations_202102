package com.psybergate.grad2021.core.collections.hw3;

import java.util.SortedSet;
import java.util.TreeSet;

public class StringReverseOrder {

  public static void main(String[] args) {
    SortedSet sortedSet = new TreeSet(new ReverseOrderComparator());

    insertStrings(sortedSet);
    printReverseOrder(sortedSet);
  }

  private static void insertStrings(SortedSet sortedSet) {

    sortedSet.add("banana");
    sortedSet.add("apples");
    sortedSet.add("mango");
    sortedSet.add("orange");
    sortedSet.add("pear");
  }

  private static void printReverseOrder(SortedSet sortedSet) {
    int position = 0;
    for (Object o : sortedSet) {
      System.out.println("O" + (position++) + " = " + o);
    }
  }
}