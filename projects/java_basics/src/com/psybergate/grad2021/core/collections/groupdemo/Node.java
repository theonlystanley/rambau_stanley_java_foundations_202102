package com.psybergate.grad2021.core.collections.groupdemo;

public class Node {

  private Object data;

  private Node next;

  private Node prev;

  public Node(Product data, Node next, Node prev) {
    this.data = data;
    this.next = next;
    this.prev = prev;
  }

  public Object getData() {
    return data;
  }

  public void setData(Product data) {
    this.data = data;
  }

  public Node getNext() {
    return next;
  }

  public void setNext(Node next) {
    this.next = next;
  }

  public Node getPrev() {
    return prev;
  }

  public void setPrev(Node prev) {
    this.prev = prev;
  }

  @Override
  public String toString() {
    return "Node{" +
            "data=" + data +
            '}';
  }
}
