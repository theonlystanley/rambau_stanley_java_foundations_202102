package com.psybergate.grad2021.core.collections.groupdemo2;

import java.util.Iterator;
import java.util.List;

public class MyListIterator implements Iterator {

  private Node cursor;

  public MyListIterator(MyLinkedList list) {
    this.cursor = list.getHead();
  }

  @Override
  public boolean hasNext() {
    return cursor.getNext() != null;
  }

  @Override
  public Object next() {
    Object data = cursor.getData();
    cursor = cursor.getNext();
    return data;
  }
}
