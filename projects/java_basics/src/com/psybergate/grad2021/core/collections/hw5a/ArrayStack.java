package com.psybergate.grad2021.core.collections.hw5a;

import java.util.Iterator;

public class ArrayStack extends AbstractArrayStack {

  private static final int DEFAULT_CAPACITY = 20;

  private Object[] stackElements = new Object[DEFAULT_CAPACITY];

  private int arrayCapacity;

  private int size = 0;

  public ArrayStack() {
    this(DEFAULT_CAPACITY);
  }

  public ArrayStack(int capacity) {
    arrayCapacity = capacity;
    stackElements = new Object[arrayCapacity];
  }

  @Override
  public void push(Object object) {
    if (size >= arrayCapacity) {
      resizeArray();
    }
    stackElements[size] = object;
    size++;
  }

  private void resizeArray() {
    arrayCapacity *= 2;  //Double the size of the array
    Object[] newStackElementsArray = new Object[arrayCapacity];
    copyStackElements(newStackElementsArray);
    stackElements = newStackElementsArray;
  }

  private void copyStackElements(Object[] newStackElementsArray) {
    for (int i = 0; i < stackElements.length; i++) {
      newStackElementsArray[i] = stackElements[i];
    }
  }

  @Override
  public Object pop() {
    Object element = stackElements[size - 1];
    stackElements[size - 1] = null;
    size--;
    return element;
  }

  @Override
  public Object get(int position) {
    if (position >= size) {
      throw new ArrayIndexOutOfBoundsException("Invalid index passed");
    }
    return stackElements[position];
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return size < 1;
  }

  @Override
  public boolean contains(Object o) {

    for (Iterator iterator = this.iterator(); iterator.hasNext(); ) {
      Object data = iterator.next();
      if (o.equals(data)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Iterator iterator() {
    return new StackIterator(this.stackElements, size - 1);
  }

  @Override
  public boolean add(Object o) {
    push(o);
    return true;
  }
}
