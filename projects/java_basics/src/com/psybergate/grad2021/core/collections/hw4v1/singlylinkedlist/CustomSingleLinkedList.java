package com.psybergate.grad2021.core.collections.hw4v1.singlylinkedlist;

import com.psybergate.grad2021.core.collections.hw4v1.CustomList;
import com.psybergate.grad2021.core.collections.hw4v1.Node;

public class CustomSingleLinkedList implements CustomList {

  private Node head;

  private Node tail;

  private int size = 0;



  @Override
  public boolean add(Object data) {
    return false;
  }

  @Override
  public boolean addFirst() {

    return false;
  }

  @Override
  public boolean addLast() {

    return false;
  }

  @Override
  public boolean isEmpty(){
    return size > 0;
  }

  @Override
  public boolean remove() {

    return false;
  }

  public int getSize() {
    return size;
  }
}
