package com.psybergate.grad2021.core.collections.hw7;

import java.util.Comparator;

public class DescendingOrderComparator implements Comparator {

  public static final DescendingOrderComparator DESCENDING_ORDER_COMPARATOR = new DescendingOrderComparator();

  @Override
  public int compare(Object o1, Object o2) {
    String s1 = (String) o1;
    String s2 = (String) o2;
    return -+1 * s1.compareTo(s2);
  }
}
