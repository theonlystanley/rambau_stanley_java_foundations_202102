package com.psybergate.grad2021.core.collections.ce1;

public class StackUtils {

  public static void main(String[] args) {
    Stack stack = new ArrayStack(2);
//    Stack stack = new ArrayStack();

    stack.push("ab");
    System.out.println("myStack.isEmpty() = " + stack.isEmpty());
    stack.push("cd");
    stack.push("ef");
    stack.push("gh");
    stack.push("ij");
    stack.push("kl");
    System.out.println("myStack.size() = " + stack.size());
    printTheStack(stack);
    System.out.println("stack.contains(\"kl\") = " + stack.contains("kl"));
    System.out.println("myStack.pop() = " + stack.pop());
    System.out.println("myStack.size() = " + stack.size());
    printTheStack(stack);
    System.out.println("myStack.get(4) = " + stack.get(4));
    System.out.println("stack.contains(\"kl\") = " + stack.contains("kl"));
  }

  private static void printTheStack(Stack stack) {
    for (Object o : stack) {
      System.out.println("o = " + o);
    }


  }
}
