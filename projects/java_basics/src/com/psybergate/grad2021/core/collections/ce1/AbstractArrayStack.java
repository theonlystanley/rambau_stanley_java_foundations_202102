package com.psybergate.grad2021.core.collections.ce1;

import java.util.Collection;

public abstract class AbstractArrayStack implements Stack {

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public boolean addAll(Collection c) {
    return false;
  }

  @Override
  public void clear() {

  }

  @Override
  public boolean remove(Object o) {
    return false;
  }

  @Override
  public boolean retainAll(Collection c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection c) {
    return false;
  }

  @Override
  public boolean containsAll(Collection c) {
    return false;
  }

  @Override
  public Object[] toArray(Object[] a) {
    return new Object[0];
  }

}
