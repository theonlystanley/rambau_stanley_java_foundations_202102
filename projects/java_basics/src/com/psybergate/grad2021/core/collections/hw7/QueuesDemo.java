package com.psybergate.grad2021.core.collections.hw7;

import java.util.PriorityQueue;
import java.util.Queue;

public class QueuesDemo {

  public static void main(String[] args) {

    priorityQueues();
  }

  public static void priorityQueues(){

    Queue pq = new PriorityQueue<>();

    pq.add(90);
    pq.add(1);
    pq.add(3123);
    pq.add(32);
    pq.add(23);

//    pq.add("abc");
//    pq.add("jkl");
//    pq.add("def");
//    pq.add("abc");
//    pq.add("vwx");

    System.out.println(pq);
    System.out.println("Initial Queue " + pq);
//    System.out.println("pq.remove(\"vwx\") = " + pq.remove("vwx"));
//    System.out.println("After Remove: ");
//    System.out.println("pq = " + pq);
//    System.out.println("Poll Method: " + pq.poll());

    System.out.println("Final Queue: " + pq);
  }
}
