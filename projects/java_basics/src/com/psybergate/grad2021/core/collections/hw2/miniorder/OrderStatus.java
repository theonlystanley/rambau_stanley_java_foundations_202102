package com.psybergate.grad2021.core.collections.hw2.miniorder;

public enum OrderStatus {

  PROCESSING, DESPATCHED, DELIVERED
}

