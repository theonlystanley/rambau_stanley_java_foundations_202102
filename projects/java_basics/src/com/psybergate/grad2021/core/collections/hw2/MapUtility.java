package com.psybergate.grad2021.core.collections.hw2;

import com.psybergate.grad2021.core.collections.hw2.miniorder.Customer;
import com.psybergate.grad2021.core.collections.hw2.miniorder.Order;

import java.util.HashMap;
import java.util.Map;

import static com.psybergate.grad2021.core.collections.hw2.miniorder.CustomerType.LOCAL;

public class MapUtility {

  private static Map map = new HashMap();

  public static void main(String[] args) {

    creatingOrders();

    printMap(new Order("001", new Customer("2", "Stan", "Rams", LOCAL)));

  }

  private static void printMap(Object obj) {
    System.out.println("Order Total = " + map.get(obj));
  }

  private static void creatingOrders() {

    Order o1 = new Order("001", new Customer("5", "Stan", "Rams", LOCAL));
    o1.addOrderItem(10, "books", 10, "BIG");
    o1.addOrderItem(50, "glue", 20, "BIG");

    Order o4 = new Order("002", new Customer("2", "Stan", "Rams", LOCAL));
    o4.addOrderItem(23, "pen", 7, "BIG");

    Order o3 = new Order("003", new Customer("6", "Stan", "Rams", LOCAL));
    o3.addOrderItem(25, "glue", 10, "BIG");

    Order o2 = new Order("004", new Customer("1", "Stan", "Rams", LOCAL));
    o2.addOrderItem(50, "books", 10, "BIG");
    o2.addOrderItem(100, "pencil", 5, "BIG");

    map.put(o1, o1.calculateTotal());
    map.put(o3, o3.calculateTotal());
    map.put(o2, o2.calculateTotal());
    map.put(o4, o4.calculateTotal());
  }
}
