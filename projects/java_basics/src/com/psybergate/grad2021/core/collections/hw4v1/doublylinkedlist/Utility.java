package com.psybergate.grad2021.core.collections.hw4v1.doublylinkedlist;

public class Utility {

  public static void main(String[] args) {
    CustomDoublyLinkedList list = new CustomDoublyLinkedList();
    list.add("vbw");
    list.add("abc");
    list.add("dbc");
    list.add("ibc");
    list.add("dqbc");
    for (Object o : list) {
      System.out.println("o = " + o);
    }
//    System.out.println("list.getSize() = " + list.getSize());
//    System.out.println("list = " + list);

  }
}
