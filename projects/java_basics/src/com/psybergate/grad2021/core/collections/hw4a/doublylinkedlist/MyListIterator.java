package com.psybergate.grad2021.core.collections.hw4a.doublylinkedlist;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyListIterator implements Iterator {

  private Node cursor;

  public MyListIterator(Node head) {
    cursor = head.getNext();
  }

  @Override
  public boolean hasNext() {
    return cursor.getData() != null;
  }

  @Override
  public Object next() {
    if (!hasNext()) {
      throw new NoSuchElementException("Iterator has no more elements");
    }
    Object data = cursor.getData();
    cursor = cursor.getNext();
    return data;
  }
}