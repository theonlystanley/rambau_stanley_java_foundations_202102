package com.psybergate.grad2021.core.collections.hw5a;

import static com.psybergate.grad2021.core.collections.hw5a.AscendingOrderComparator.*;

public class StackUtils {

  public static void main(String[] args) {
    SortedStack sortedStack = new TreeStack();
//    SortedStack sortedStack = new TreeStack(ASCENDING_ORDER_COMPARATOR);

    sortedStack.add(new Customer(3));

//    sortedStack.push("gb");
//    System.out.println("myStack.isEmpty() = " + sortedStack.isEmpty());
//    sortedStack.push("ad");
//    sortedStack.push("zh");
//    sortedStack.push("ef");
//    sortedStack.push("kl");
//    sortedStack.push("bj");
//    System.out.println("myStack.size() = " + sortedStack.size());
//    printTheStack(sortedStack);
//    System.out.println("sortedStack.contains(\"kl\") = " + sortedStack.contains("kl"));
//    System.out.println("myStack.pop() = " + sortedStack.pop());
//    System.out.println("myStack.size() = " + sortedStack.size());
    printTheStack(sortedStack);
//    System.out.println("myStack.get(4) = " + sortedStack.get(4));
//    System.out.println("sortedStack.contains(\"kl\") = " + sortedStack.contains("kl"));
  }

  private static void printTheStack(Stack stack) {
    for (Object o : stack) {
      System.out.println("o = " + o);
    }
  }
}
