package com.psybergate.grad2021.core.collections.ce1;

import java.util.Iterator;

public class StackIterator implements Iterator {

  private Object[] stackElements;
  private int cursor = 0;

  public StackIterator(Object[] stackElements, int cursor) {
    this.stackElements = stackElements;
    this.cursor = cursor;
  }

  @Override
  public boolean hasNext() {
    return cursor >= 0;
  }

  @Override
  public Object next() {
    return stackElements[cursor--];
  }
}

