package com.psybergate.grad2021.core.collections.hw2.miniorder;


public class InternationalOrder extends Order {

  public static final String INTERNATIONAL_ORDER = "International Order";
  /**
   *  Amount in Rands
   */
  private double importDuties;

  public InternationalOrder(String orderNum, Customer customer, OrderStatus orderStatus) {
    super(orderNum, customer);
    if (customer.getCustomerType() != CustomerType.INTERNATIONAL) {
      throw new RuntimeException("Invalid Customer: Only International Customer Can Order Internationally");
    }
  }

  @Override
  public boolean isInternational() {
    return false;
  }

  @Override
  public double calculateTotal() {
    return super.calculateTotal() + importDuties;
  }

  public double getImportDuties() {
    return importDuties;
  }
}
