package com.psybergate.grad2021.core.collections.hw1;

import java.time.LocalDate;

public class Customer implements Comparable {

  private int customerNum;

  private String customerName;

  private String surname;

  private CustomerType customerType;

  private LocalDate registrationDate;

  public Customer(int customerNum, String customerName, String surname, CustomerType customerType) {
    this(customerNum, customerName, surname, LocalDate.now(), customerType); /// This one defaults the start date to the current date.
  }

  public Customer(int customerNum, String customerName, String surname, LocalDate registrationDate, CustomerType customerType) {
    this.customerNum = customerNum;
    this.customerName = customerName;
    this.surname = surname;
    this.registrationDate = registrationDate; // Set the creation date to the current date
    this.customerType = customerType;
  }

  public int getCustomerNum() {
    return customerNum;
  }

  public CustomerType getCustomerType() {
    return customerType;
  }

  @Override
  public String toString() {
    return "Customer{" +
            ", CustomerNum= "+customerNum+
            ", customerName='" + customerName + '\'' +
            ", surname='" + surname + '\'' +
            '}';
  }

  @Override
  public int compareTo(Object o) {
    return this.customerNum-((Customer)o).customerNum;
  }
}

