package com.psybergate.grad2021.core.collections.hw5a;

import java.util.Comparator;

public interface SortedStack extends Stack{

  Comparator comparator();
}
