package com.psybergate.grad2021.core.collections.otherstructures.nodesimp;

public class Node {

  Object element;
  Node parent;
  Node leftChild;
  Node rightChild;

  public Node(Node parent, Node leftChild, Node rightChild, Object element) {
    this.parent = parent;
    this.leftChild = leftChild;
    this.rightChild = rightChild;
    this.element = element;
  }

}
