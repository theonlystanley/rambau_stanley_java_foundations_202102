package com.psybergate.grad2021.core.collections.hw4v1;

public interface CustomList {

  boolean add(Object data);

  boolean addFirst();

  boolean addLast();

  boolean remove();

  boolean isEmpty();
}
