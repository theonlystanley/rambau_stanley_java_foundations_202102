package com.psybergate.grad2021.core.collections.hw5a;

import java.util.Comparator;
import java.util.Iterator;

public class TreeStack extends AbstractTreeStack implements SortedStack {

  private static final int DEFAULT_CAPACITY = 20;

  private Object[] stackElements = new Object[DEFAULT_CAPACITY];

  private int arrayCapacity;

  private int size = 0;

  private Comparator comparator = null;

  public TreeStack() {
    this(DEFAULT_CAPACITY, null);
  }

  public TreeStack(int capacity) {
    this(capacity, null);
  }

  public TreeStack(Comparator comparator) {
    this(DEFAULT_CAPACITY, comparator);
  }

  private TreeStack(int capacity, Comparator comparator) {
    this.arrayCapacity = capacity;
    this.comparator = comparator;
    this.stackElements = new Object[arrayCapacity];
  }

  @Override
  public void push(Object object) {
    if (size >= arrayCapacity) {
      resizeArray();
    }
    sortElements(object);
    size++;
  }

  private void sortElements(Object object) {
    int lastIndex = size;
    if (comparator == null) {
      //Arrays.sort(stackElements, 0, lastIndex);
      naturalOrdering(object);
    } else {
      //Arrays.sort(stackElements, 0, lastIndex, comparator);
      comparatorOrdering(object);
    }
  }

  private void comparatorOrdering(Object object) {
    for (int i = 0; i < size; i++) {
      if (comparator.compare(object, stackElements[i]) < 0) {
        rightShiftTheElements(i);
        stackElements[i] = object;
        return;
      }
    }
    stackElements[size] = object;
  }

  private void naturalOrdering(Object object) {
    Comparable comparableObject = (Comparable) object;
    for (int i = 0; i < size; i++) {
      boolean compare = comparableObject.compareTo(stackElements[i]) < 0;
      if (compare) {
        rightShiftTheElements(i);
        stackElements[i] = object;
        return;
      }
    }
    stackElements[size] = object;
  }

  private void rightShiftTheElements(int i) {
    for (int j = size -1 ; j >= i; j--) {
      stackElements[j + 1] = stackElements[j];
    }
  }

  private void resizeArray() {
    arrayCapacity *= 2;  //Double the size of the array
    Object[] newStackElementsArray = new Object[arrayCapacity];
    copyStackElements(newStackElementsArray);
    stackElements = newStackElementsArray;
  }

  private void copyStackElements(Object[] newStackElementsArray) {
    for (int i = 0; i < stackElements.length; i++) {
      newStackElementsArray[i] = stackElements[i];
    }
  }

  @Override
  public Object pop() {
    Object element = stackElements[size - 1];
    stackElements[size - 1] = null;
    size--;
    return element;
  }

  @Override
  public Object get(int position) {
    if (position >= size) {
      throw new ArrayIndexOutOfBoundsException("Invalid index passed");
    }
    return stackElements[position];
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return size < 1;
  }

  @Override
  public boolean contains(Object o) {

    for (Iterator iterator = this.iterator(); iterator.hasNext(); ) {
      Object data = iterator.next();
      if (o.equals(data)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Iterator iterator() {
    return new StackIterator(this.stackElements, size - 1);
  }

  @Override
  public boolean add(Object o) {
    push(o);
    return true;
  }

  @Override
  public Comparator comparator() {
    return comparator;
  }
}
