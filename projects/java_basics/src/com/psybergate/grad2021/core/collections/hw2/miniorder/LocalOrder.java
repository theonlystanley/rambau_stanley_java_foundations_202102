package com.psybergate.grad2021.core.collections.hw2.miniorder;

public class LocalOrder extends Order {

  public static final String LOCAL_ORDER = "Local Order";

  public LocalOrder(String orderNum, Customer customer, OrderStatus orderStatus) {
    super(orderNum, customer);
    if (customer.getCustomerType() != CustomerType.LOCAL) { // change to an equals
      throw new RuntimeException("Invalid Customer: Only Local Customer Can Order Locally");
    }
  }

  @Override
  public boolean isLocal() {
    return true;
  }

  @Override
  public double calculateTotal() {
    return super.calculateTotal() * (1 + getDiscount());
  }

  public double getDiscount() {
    return getDiscount();
  }
}
