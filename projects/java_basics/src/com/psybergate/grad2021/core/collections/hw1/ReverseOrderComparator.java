package com.psybergate.grad2021.core.collections.hw1;

import java.util.Comparator;

public class ReverseOrderComparator implements Comparator {

  public static final ReverseOrderComparator REVERSE_ORDER_COMPARATOR = new ReverseOrderComparator();

  public ReverseOrderComparator() {

  }

  @Override
  public int compare(Object o1, Object o2) {
    Customer c1 = (Customer) o1;
    Customer c2 = (Customer) o2;
    return c2.getCustomerNum() - c1.getCustomerNum();
  }
}
