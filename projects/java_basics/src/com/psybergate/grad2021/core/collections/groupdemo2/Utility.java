package com.psybergate.grad2021.core.collections.groupdemo2;

import java.util.List;

public class Utility {

  public static void main(String[] args) {
    List list = new MyLinkedList();

    list.add("ab");
    list.add("cd");
    list.add("ef");
    list.add("gh");

    print(list);
  }

  private static void print(List list) {
    for (Object o : list) {
      System.out.println("o = " + o);
    }
  }
}
