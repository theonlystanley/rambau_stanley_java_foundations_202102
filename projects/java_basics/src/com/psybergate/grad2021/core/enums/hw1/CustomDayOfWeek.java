package com.psybergate.grad2021.core.enums.hw1;

public class CustomDayOfWeek {

  public static final CustomDayOfWeek MONDAY = new CustomDayOfWeek("MON");

  public static final CustomDayOfWeek TUESDAY = new CustomDayOfWeek("TUE");

  public static final CustomDayOfWeek WEDNESDAY = new CustomDayOfWeek("WED");

  public static final CustomDayOfWeek THURSDAY = new CustomDayOfWeek("THU");

  private String shortName;

  private CustomDayOfWeek() {
  }

  public CustomDayOfWeek(String shortName) {
    this.shortName = shortName;
  }
}
