package com.psybergate.grad2021.core.enums.hw1;

public enum DayOfWeek {
  MONDAY("MON"), TUESDAY("TUE"), WEDNESDAY("WED"), THURSDAY("THU");

  private String shortName;

  private DayOfWeek(String shortName) {
    this.shortName = shortName;
  }
}
