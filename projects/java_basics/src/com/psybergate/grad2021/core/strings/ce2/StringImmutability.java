package com.psybergate.grad2021.core.strings.ce2;

public class StringImmutability {

  public static void main(String[] args) {

    String s1 = "hello";
    String s2 = "helloWorld";
    String s3 = new String("hello").intern();

    System.out.println(s1 == s2);
    System.out.println(s2 == s3);
    System.out.println(s1 == s3);

    s1.concat("World");
    System.out.println(s1);

    String s4 = s1.concat("World");
    System.out.println(s4);
    System.out.println("s2 = " + (s2==s4));

  }
}
