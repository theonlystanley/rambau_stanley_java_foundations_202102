package com.psybergate.grad2021.core.strings.hw1;

public class StringFormatter {

  public static void main(String[] args) {

    String s = String.format("The correct answer is %s", false);
    System.out.println("s = " + s);

    String s1 = String.format("The number 25 in decimal = %d", 25);
    System.out.println("s1 = " + s1);

    String s2 = String.format("Without left justified flag: %5d", 25);
    System.out.println("s2 = " + s2);

    String s4 = String.format("Output of 25.09878 with Precision 2: %.2f", 25.09878);
    System.out.println("s4 = " + s4);

    System.out.printf("Output of 25.09878 with Precision 2: %.2f", 25.09878);
  }
}
