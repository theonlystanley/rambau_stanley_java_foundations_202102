package com.psybergate.grad2021.core.strings.hw2;

import java.util.StringTokenizer;

public class Tokenizing {

  public static void main(String[] args) {

    String line = "This is a test line for string tokenizing";

    StringTokenizer lineTokens = new StringTokenizer(line);

    System.out.println("Example 1: ");
    while(lineTokens.hasMoreTokens()){
      System.out.println(lineTokens.nextToken());
    }

    line = "This is, a test line@ for string, tokenizing";

    StringTokenizer lineTokens2 = new StringTokenizer(line, "@,");

    System.out.println("Example 2: ");
    while(lineTokens2.hasMoreTokens()){
      System.out.println(lineTokens2.nextToken());
    }
  }
}
