package com.psybergate.grad2021.core.strings.hw3;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parsing {

  public static void main(String[] args) {

    String line = "This string line will be used to test parsing of strings in java";

    System.out.println("Using split method: ");
    String[] splitArray1 = line.split(" ");
    for (int i = 0; i < splitArray1.length; i++) {
      System.out.println("splitArray1[" + i + "] = " + splitArray1[i]);
    }

    System.out.println("Using split method: with a limit");
    String[] splitArray2 = line.split(" ", 4);
    for (int i = 0; i < splitArray2.length; i++) {
      System.out.println("splitArray1[" + i + "] = " + splitArray2[i]);
    }

    StringTokenizer lineTokens = new StringTokenizer(line, " ");

    System.out.println("Using StringTokenizer: ");
    while (lineTokens.hasMoreTokens()) {
      System.out.println("Token -> " + lineTokens.nextToken());
    }

    System.out.println("Using Regular expressions: ");
    Pattern datePattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
    String date = "2020-04-24";
    Matcher matcher = datePattern.matcher(date);
    System.out.println(date +" date format is: "+ (matcher.matches() ? "Correct" : "Incorrect"));

  }
}
