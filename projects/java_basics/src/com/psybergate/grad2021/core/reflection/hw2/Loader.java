package com.psybergate.grad2021.core.reflection.hw2;

import com.psybergate.grad2021.core.strings.ce2.StringImmutability;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ThreadPoolExecutor;

public class Loader {
  public static void main(String[] args) throws IOException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {

//    for (Object key : PROPERTIES.keySet()) {
//      Class<List<String>> clazz = getClass((String) key);
//      List<String> months = createAListWithData(clazz);
//      printList(months);
//      System.out.println("");
//    }

    Class<List<String>> clazz = getClass("list");
    System.out.println("clazz.getSimpleName() = " + clazz.getSimpleName());
    List<String> months = createAListWithData(clazz);
    printList(months);
  }

  private static final Properties PROPERTIES = new Properties();

  public static void loadStream() throws IOException {
    ClassLoader classLoader = Loader.class.getClassLoader();
    InputStream stream = classLoader.getResourceAsStream("app.properties");
    PROPERTIES.load(stream);
  }

  static {
    try {
      loadStream();
    } catch (IOException e) {
      throw new RuntimeException();
    }
  }

  public static Class getClass(String listName) throws ClassNotFoundException {
    return Class.forName(getProperty(listName));
  }

  static String getProperty(String key) {
    return (String) PROPERTIES.get(key);
  }

  public static List<String> createAListWithData(Class<List<String>> clazz) throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {

    List<String> list = clazz.newInstance();
    list.add("January");
    list.add("February");
    list.add("March");
    list.add("April");
    list.add("May");
    list.add("June");
    list.add("July");
    list.add("August");
    list.add("September");
    list.add("October");
    list.add("November");
    list.add("December");

    return list;
  }

  public static void printList(List<String> list) {
    int i = 1;
    for (String month : list) {
      System.out.println(i++ + ".) " + month);
    }
  }

}
