package com.psybergate.grad2021.core.reflection;

import java.io.Serializable;
import java.time.LocalDate;

public class Student implements Serializable {

  private String studentNumber;

  private String name;

  private int age;

  private String gender;

  private LocalDate dateOfBirth;

  public Student() {
  }

  public Student(String studentNumber, String name, String gender, LocalDate dateOfBirth) {
    this.studentNumber = studentNumber;
    this.name = name;
    this.gender = gender;
    this.dateOfBirth = dateOfBirth;
  }

  public static void doSomething(){
    System.out.println("Doing something...");
  }

  public String calcAge(){
    return (LocalDate.now().minusYears(dateOfBirth.getYear())).toString();
  }

  public String getStudentNumber() {
    return studentNumber;
  }

  public void setStudentNumber(String studentNumber) {
    this.studentNumber = studentNumber;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(LocalDate dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }
}
