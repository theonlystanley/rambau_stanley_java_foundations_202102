package com.psybergate.grad2021.core.reflection.hw1;

import com.psybergate.grad2021.core.reflection.Student;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Utility {
  public static void main(String[] args) {
    Class clazz = Student.class;

    printSuperClass(clazz);

    printClassModifiers(clazz);

    printFieldModifiers(clazz);

    printMethodModifiers(clazz);

  }

  private static void printClassModifiers(Class clazz) {
    System.out.println("Class: " + clazz.getSimpleName() + ", " + Modifier.toString(clazz.getModifiers()));
  }

  private static void printMethodModifiers(Class clazz) {
    for (Method method : clazz.getDeclaredMethods()) {
      String methodModifier = "Method: " + method.getName() + ", " + Modifier.toString(method.getModifiers());
      print(methodModifier);
    }
  }

  private static void printFieldModifiers(Class clazz) {
    for (Field field : clazz.getDeclaredFields()) {
      String fieldModifier = "Field: " + field.getName() + ",  " + Modifier.toString(field.getModifiers());
      print(fieldModifier);
    }
  }

  private static void printSuperClass(Class clazz) {
    String superClassName = "Superclass: " + clazz.getSuperclass().getSimpleName();
    print(superClassName);
  }

  public static void print(String message) {
    System.out.println(message + "\n");
  }

}
