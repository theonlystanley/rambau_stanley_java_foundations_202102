package com.psybergate.grad2021.core.reflection.ce2;

import com.psybergate.grad2021.core.reflection.Student;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;

public class Utility {

  public static void main(String[] args) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

    Class clazz = Student.class;

    Constructor<Student> constructor1 = clazz.getConstructor();
    Constructor<Student> constructor2 = clazz.getConstructor(String.class, String.class, String.class, LocalDate.class);

    Student student1 = constructor1.newInstance();
    Student student2 = constructor2
            .newInstance("12356789", "John", "Male", LocalDate.of(1995,06,12));

    Method method = clazz.getMethod("setName", String.class);
    method.invoke(student1, "Reflection");

    System.out.println("Student 1 name : "+student1.getName());
    System.out.println("Student 2 number :  " + student2.getStudentNumber());

  }
}
