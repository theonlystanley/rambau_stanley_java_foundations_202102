package com.psybergate.grad2021.core.reflection.hw3;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

public class Utility {

  public static void main(String[] args) throws Throwable {

    MethodHandles.Lookup publicLookup1 = MethodHandles.publicLookup();
    MethodType mt1 = MethodType.methodType(String.class, char.class, char.class);
    MethodHandle replaceMH = publicLookup1.findVirtual(String.class, "replace", mt1);
    String output = (String) replaceMH.invoke("jovo", Character.valueOf('o'), 'a');
    System.out.println("output = " + output);

//    MethodHandles.Lookup publicLookup2 = MethodHandles.publicLookup();
//    MethodType mt2 = MethodType.methodType(void.class);
//    MethodHandle doSomethingMH = publicLookup1.findVirtual(Customer.class, "doSomething", mt2);
//    doSomethingMH.invoke();

    MethodHandles.Lookup lookup3 = MethodHandles.publicLookup();
    MethodType mt3 = MethodType.methodType(int.class, int.class, int.class);
    MethodHandle sumMH = lookup3.findStatic(Integer.class, "sum", mt3);
    int sum = (int) sumMH.invokeExact(1, 11);
    System.out.println(sum);

    MethodHandles.Lookup lookup4 = MethodHandles.publicLookup();
    MethodType mt4 = MethodType.methodType(void.class);
    MethodHandle constMH = lookup4.findConstructor(Customer.class, mt4);
    constMH.invoke();

  }
}
