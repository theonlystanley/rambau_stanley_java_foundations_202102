package com.psybergate.grad2021.core.reflection.hw3;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.time.LocalDate;

public class Customer {

  private String studentNumber;

  private String name;

  private String surname;

  private LocalDate dateOfBirth;

  public Customer() {}

  public Customer(String studentNumber, String name, String surname, LocalDate dateOfBirth) {
    this.studentNumber = studentNumber;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;

  }

  public String getName() {
    return name;
  }

  public void doSomething(){
    System.out.println("Doing Something...");
  }

  public int getAge() {
    System.out.println("Calculating age...");
    return 19;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }
}
