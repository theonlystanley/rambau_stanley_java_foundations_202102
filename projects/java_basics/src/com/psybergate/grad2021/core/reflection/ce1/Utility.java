package com.psybergate.grad2021.core.reflection.ce1;

import com.psybergate.grad2021.core.reflection.Student;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class Utility {

  public static void main(String[] args) {

    Class studentClass = Student.class;

    System.out.println("Class name: " + studentClass.getSimpleName());
    System.out.println("Superclass name: " + studentClass.getSuperclass().getSimpleName());

    System.out.println("Package name: " + studentClass.getPackage().getName());

    Annotation[] annotations = studentClass.getAnnotations();
    for (Annotation annotation : annotations) {
      System.out.println("Annotation name: " + annotation.annotationType().getSimpleName());
    }

    for (Class interfaceClass : studentClass.getInterfaces()) {
      System.out.println("Interface name: " + interfaceClass.getSimpleName());
    }

    for (Field field : studentClass.getFields()) {
      System.out.println("Public field name: " + field.getName());
    }

    for (Field field : studentClass.getDeclaredFields()) {
      System.out.println("Field name: " + field.getName());
    }

    int i = 1;
    for (Constructor constructor : studentClass.getConstructors()) {
      System.out.println("Parameters for Constructor: " + i++);
      for (Parameter parameter : constructor.getParameters()) {
        System.out.println(parameter.getType().getSimpleName());
      }
    }

    for (Method method : studentClass.getMethods()) {
      System.out.println("Method: " + method.getName() + "; Return Type: " + method.getReturnType().getSimpleName());
    }
  }
}

