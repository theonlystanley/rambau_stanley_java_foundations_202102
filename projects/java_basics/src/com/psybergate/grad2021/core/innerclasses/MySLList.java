package com.psybergate.grad2021.core.innerclasses;

import java.util.Iterator;

public class MySLList {

  private Node head;

  private Node last;

  public MySLList(Node head) {
    this.head = head;
    this.last = head;
  }

  public void add(Object element) {}

  private static class Node {

    Object element;

    Node next;

    public Node() {
    }

  }

  private class MyIterator implements Iterator {

    @Override
    public boolean hasNext() {
      return false;
    }

    @Override
    public Object next() {
      return null;
    }
  }
}
