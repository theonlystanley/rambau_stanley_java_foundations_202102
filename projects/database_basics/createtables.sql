DROP TABLE IF exists transaction, account, customer, accountType, transactionType;

CREATE TABLE customer (
    id SERIAL PRIMARY KEY NOT NULL,
    customerNum int NOT NULL,
    customerName VARCHAR(55) NOT NULL,
    customerSurname VARCHAR(55),
    customerDateOfBirth DATE NOT NULL
);

CREATE TABLE account(
    id SERIAL PRIMARY KEY NOT NULL,
    customerId int not null,
    accountNum int not null,
    accountBalance int not null,
    accountType int not null,
    constraint fk_01 foreign key (customerId) references customer
);

CREATE TABLE accountType(
    id serial primary key not null,
    acc_type varchar not null
);

CREATE TABLE transactionType(
    id serial primary key not null,
    trans_type varchar not null 
);

CREATE TABLE transaction (
    transactionNum SERIAL NOT NULL,
    accountId int not null,
    transactionTypeId int not null,
    transactionDate date not null,
    constraint pk_05 primary key (transactionNum, accountId, transactionTypeId),
    constraint fk_02 foreign key (accountId) references account,
    constraint fk_03 foreign key (transactionTypeId) references transactionType
);

