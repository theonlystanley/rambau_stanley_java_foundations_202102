alter table account add column needstoBeReviewed boolean not null default false;
update account set needstoBeReviewed=true where accountbalance<2000;